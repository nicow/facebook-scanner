<?php

namespace AppBundle\Command;

use AppBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName("app:test");
    }
    public function execute(InputInterface $input, OutputInterface $output)
    {


        $facebook = $this->getContainer()->get('app.facebook');
        $page = new Page();
        $page->setFacebookID('5027904559');

        $posts = $facebook->getPageData($page, ['posts.limit(10)'])->getResult()['posts']['data'];

        $total_count = 0;

        $likes = $facebook->getPageData($page, ['fan_count'])->getResult()['fan_count'];


        foreach ($posts as $post) {
            foreach ($facebook->getReactionsForPostCount($post['id']) as $id => $count) {
                $total_count = $total_count + $count;
            }
        }

        $rate = ($total_count/count($posts)/$likes) * 100;
        $raten[10] = $rate;



        $output->writeln(json_encode(['rate'=>$rate,'reactions'=>$total_count,'posts'=>count($posts)]));

    }
}