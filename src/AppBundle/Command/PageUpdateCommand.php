<?php

namespace AppBundle\Command;

use AppBundle\Entity\Page;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use ColourStream\Bundle\CronBundle\Annotation\CronJob;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 21.06.2017
 * Time: 09:29
 * @Cronjob(interval="PT1M")
 *
 */
class PageUpdateCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName("app:page:update")
        ->setDescription('update empty page names');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $pages = $em->getRepository('AppBundle:Page')->findAllWithoutName();

        $facebook = $this->getContainer()->get('app.facebook');

        /**
         * @var Page $page
         */
        foreach ($pages as $page) {
            $data = $facebook->getData($page->getFacebookID(), ['name'])->getResult();
            $page->setName($data['name']);
            $page->setCleanName($page->getName());
            $em->flush();
        }

        $output->writeln("Done!");

    }


}