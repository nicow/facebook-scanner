<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 21.06.2017
 * Time: 12:36
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MaintenanceOnCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName("app:maintenance:on")
            ->setDescription("Enables the Maintenance mode");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $filepath = $this->getContainer()->getParameter('maintenance_file_path');
        if (file_exists($filepath)) {
            $output->writeln("Maintenance is already enabled!");
        } else {
            touch($filepath);
            $output->writeln("Maintenance mode is no enabled");
        }

    }
}