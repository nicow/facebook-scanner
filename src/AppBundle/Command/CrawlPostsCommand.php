<?php
/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 15.08.2017
 * Time: 16:10
 */

namespace AppBundle\Command;

use AppBundle\Entity\Page;
use AppBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use ColourStream\Bundle\CronBundle\Annotation\CronJob;

/**
 * Class CrawlPostsCommand
 * @package AppBundle\Command

 */
class CrawlPostsCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName("app:search:posts")
            ->setDescription('Search posts');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $repo = $em->getRepository('AppBundle:Search');
        $postrepo = $em->getRepository('AppBundle:Post');
        $search = $repo->getNextSearch();

        $repoApp = $em->getRepository('AppBundle:App');
        $apps = $repoApp->findBy(['blocked' => false, 'inUse' => false]);


        if (!$apps || $apps == []) {
            return 0;
        } else {
            shuffle($apps);
//            $apps[0]->setInUse(true);
            $em->flush();
        }


        $pages = $em->getRepository('AppBundle\Entity\Page')->findAllExceptDeleted();

        shuffle($pages);


        if (count($pages) == 0) {
            return 1;
        }


        $fb = $this->getContainer()->get('app.facebook');
        /**
         * @var Page $page
         */
        foreach ($pages as $id => $page) {
            try {
                $data = $fb->getData($page->getFacebookID() . '/posts')->getResult();
                $output->writeln($id . '::' . $page->getName());
                foreach ($data['data'] as $newPost) {

                    if (isset($newPost['id']) && !$postrepo->findOneBy(['facebookId' => $newPost['id']])) {
                        $post = new Post();
                        $post->setFacebookId($newPost['id']);
                        @$post->setMessage($newPost['message']);
                        $post->setCreatedTime(\DateTime::createFromFormat(DATE_ISO8601, $newPost['created_time']));
                        $post->setPage($page);
                        $em->persist($post);
                        $em->flush();


                    }
                }
            } catch (\Exception $exception) {
                continue;
            }
        }

    }

}