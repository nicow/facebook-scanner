<?php
namespace AppBundle\Command;

use AppBundle\Entity\Customer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 18.06.2017
 * Time: 21:11
 */

class CreateUserCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName("app:user:create")
            ->setDescription("Creates a new User for this app");
    }

    public function  execute(InputInterface $input, OutputInterface $output)
    {
        $userManager = $this->getContainer()->get('fos_user.user_manager');

        $helper = $this->getHelper('question');

        $emailQuestion = new Question("Please enter an email:");
        $usernameQuestion = new Question("Please enter an username:");
        $passwordQuestion = new Question("Please enter a password:");
        $fullNameQuestion = new Question("Please enter the full name of the new user:");

        $email = $helper->ask($input, $output, $emailQuestion);

        if ($userManager->findUserByEmail($email)) {
            $output->writeln("Email already in use!");
        } else {
            $username = $helper->ask($input, $output, $usernameQuestion);
            $fullname = $helper->ask($input, $output, $fullNameQuestion);
            $password = $helper->ask($input, $output, $passwordQuestion);

            /**
             * @var Customer $user
             */
            $user = $userManager->createUser();

            $user->setEmail($email);
            $user->setPlainPassword($password);
            $user->setUsername($username);
            $user->setFullname($fullname);
            $user->setEnabled(1);

            $userManager->updateUser($user);

            $output->writeln("User created!");
        }




    }
}