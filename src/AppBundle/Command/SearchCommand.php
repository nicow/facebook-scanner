<?php

namespace AppBundle\Command;

use AppBundle\Entity\Customer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ColourStream\Bundle\CronBundle\Annotation\CronJob;


/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16.06.2017
 * Time: 17:50
 */

/**
 * Class SearchCommand
 * @package AppBundle\Command
 */
class SearchCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName('app:search')
            ->setDescription('Update pages in our Database')
            ->addArgument('search', InputArgument::OPTIONAL,'Search only for this term');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {

        /**
         * @var ObjectManager $em
         */
        $em = $this->getContainer()->get('doctrine')->getManager();

        if ($input->hasArgument('search')) {
            $oneTerm = $input->getArgument('search');
        } else {
            $oneTerm = null;
        }

        $searchRepo = $em->getRepository('AppBundle:Search');
        $pageRepo = $em->getRepository('AppBundle:Page');
        $facebook = $this->getContainer()->get('app.facebook');

        $helper = $this->getContainer()->get('app.helper.page');

        $fields = [
            'location{city,country,zip,street}',
            'about',
            'fan_count',
            'is_verified',
            'link',
            'talking_about_count',
            'category',
            'displayed_message_response_time',
            'were_here_count',
            'description',
            'cover{source}',
            'name',
            'phone',
            'impressum',
            'username',
            'website',
            'picture.width(256).height(256){url}'
        ];


        if ($oneTerm) {
            $jobs = $searchRepo->findBy(['term' => $oneTerm]);
        } else{
            $jobs = $searchRepo->findAll();
        }

        foreach ($jobs as $job) {
            $success = $failure = 0;

            $after = $job->getAfter();
            if ($after == "NONE") {
                $after = null;
            }
            $pages = [];

            do {

                if ($after) {
                    $data = $facebook->search($job->getTerm(), 'page', $this->getContainer()->getParameter('search_default_per_page'), $fields,$after)->getResult();
                } else {
                    $data = $facebook->search($job->getTerm(), 'page', $this->getContainer()->getParameter('search_default_per_page'), $fields)->getResult();
                }

                foreach ($data['data'] as $page) {
                    $pages[] = $page;
                }

                if (isset($data['paging'])) {
                    if (isset($data['paging']['cursors'])) {
                        if (isset($data['paging']['cursors']['after'])) {
                            $after = $data['paging']['cursors']['after'];
                        } else {
                            $after = null;
                        }
                    }
                } else {
                    $after = null;
                }

            } while ($after != null && count($pages) < $this->getContainer()->getParameter('search_max_per_iteration'));

            if ($after) {
                $job->setAfter($after);
            } else {
                $job->setLastComplete((new \DateTime()));
                $job->setAfter("NONE");
            }

            $em->flush();

            foreach ($pages as $item) {

                $page = $pageRepo->findOneBy(['facebookID' => $item['id'],'customer'=>$job->getCustomer()]);
                if (!$page) {
                    $page = $helper->getNewPage($item['id']);
                    $page->setSearch($job);
                }

                if ($page->isDisabled()) {
                    continue;
                }

                /**
                 * @var Customer $user
                 */
                $user = $job->getCustomer();

                $page = $helper->fillPage($page, $item, $user->getSearchCountries(), $user->isUseResultsWithoutCountry());

                if (!$page) {
                    continue;
                }
                if (!$page->getId()) {
                    if ($user->getMaxPages() > count($pageRepo->findBy(['customer' => $user]))) {
                        $em->persist($page);
                        $success++;
                    } else {
                        $failure++;
                        unset($page);
                    }

                }

                if (isset($page)) {
                    $page->setCustomer($user);
                }


                $em->flush();
            }

            $output->write("Es wurden " . count($pages) . ' Treffer f�r: ' . $job->getTerm() ." gefunden." . $success . " Seiten wurden hinzugef�gt.");
            if ($failure > 0) {
                $output->writeln($failure . ' Seiten �ber Kontingent!');
            } else {
                $output->writeln("");
            }
        }

        return true;

    }
}