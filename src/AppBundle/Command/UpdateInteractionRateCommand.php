<?php
/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 26.07.2017
 * Time: 15:46
 */

namespace AppBundle\Command;

use AppBundle\Entity\Page;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ColourStream\Bundle\CronBundle\Annotation\CronJob;

/**
 * Class UpdateInteractionRateCommand
 * @package AppBundle\Command
 * @Cronjob(interval="P1D")
 */
class UpdateInteractionRateCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this->setName('app:page:interactionrateupdate')
        ->setDescription('UPdate Interactionrate');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        mail("asdf@crea.science", "inter start", "starte cronjob");
        $em = $this->getContainer()->get('doctrine')->getManager();
        $facebook = $this->getContainer()->get('app.facebook');
        /**
         * @var QueryBuilder $qb
         */
        $qb= $em->getRepository('AppBundle\Entity\Page')->createQueryBuilder('p');


        $baseQuery = $qb->where('p.search is not null')->where('p.deleted = false')->andWhere('p.read = true')->andWhere('p.disabled = false');

        $pages = $baseQuery->andWhere($qb->expr()->isNull('p.interactionrate'))

            ->getQuery()->getResult();
        /**
         * @var array $pages
         */
        /**
         * @var Page $page
         */
        foreach ($pages as $pid => $page) {

            $posts = $facebook->getPageData($page, ['posts.limit(20)'])->getResult();

            if (!isset($posts['posts'])) {
                continue;
            }

            $posts = $posts['posts']['data'];

            $total_count = 0;

            $likes = $facebook->getPageData($page, ['fan_count'])->getResult()['fan_count'];


            foreach ($posts as $post) {
                foreach ($facebook->getReactionsForPostCount($post['id']) as $id => $count) {
                    $total_count = $total_count + $count;
                }
            }

            $rate = ($total_count / count($posts) / $likes) * 100;

            $page->setInteractionrate($rate);
            $page->setScrapeDate((new \DateTime()));
            $em->flush();
            $output->writeln($pid."::Update Page: " . $page->getName() . ': ' . $rate);
        }



        $pages = $baseQuery->andWhere($qb->expr()->isNotNull('p.interactionrate'))
            ->orderBy('p.scrapedate','DESC')
            ->getQuery()->getResult();
        /**
         * @var array $pages
         */
        /**
         * @var Page $page
         */
        foreach ($pages as $pid => $page) {

            $posts = $facebook->getPageData($page, ['posts.limit(20)'])->getResult();

            if (!isset($posts['posts'])) {
                continue;
            }

            $posts = $posts['posts']['data'];

            $total_count = 0;

            $likes = $facebook->getPageData($page, ['fan_count'])->getResult()['fan_count'];


            foreach ($posts as $post) {
                foreach ($facebook->getReactionsForPostCount($post['id']) as $id => $count) {
                    $total_count = $total_count + $count;
                }
            }

            $rate = ($total_count / count($posts) / $likes) * 100;

            $page->setInteractionrate($rate);
            $page->setScrapeDate((new \DateTime()));
            $em->flush();
            $output->writeln($pid."::Update Page: " . $page->getName() . ': ' . $rate);
        }

        mail("asdf@crea.science", "inter end", "ende cronjob");

    }

}
