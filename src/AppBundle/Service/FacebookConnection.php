<?php

namespace AppBundle\Service;

use Facebook\Facebook as FB;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FacebookConnection
 * @package AppBundle\Service
 */
class FacebookConnection
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var FB
     */
    private $facebook;

    /**
     * FacebookConnectionHelper constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->setFacebook((new FB([
            'app_id' => $this->container->getParameter('facebook_app_id'),
            'app_secret' => $this->container->getParameter('facebook_app_secret'),
            'default_access_token' => $this->container->getParameter('facebook_app_token'),
            'locale' => $this->container->getParameter('facebook_app_locale')
        ])));
    }

    /**
     * @return FB
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    public function getLocale()
    {
        return $this->container->getParameter('facebook_app_locale');
    }

    /**
     * @param FB $facebook
     */
    private function setFacebook(FB $facebook)
    {
        $this->facebook = $facebook;
    }

}