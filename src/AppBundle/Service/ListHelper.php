<?php

namespace AppBundle\Service;

use AppBundle\Entity\PageList;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 18.06.2017
 * Time: 16:06
 */

class ListHelper
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function countPagesInList(PageList $list)
    {
        return count($this
            ->container
            ->get('doctrine')
            ->getManager()
            ->getRepository('AppBundle:Page')
            ->findAllExceptDeletedInList($list));
    }

}