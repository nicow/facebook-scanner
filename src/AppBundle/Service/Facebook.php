<?php

namespace AppBundle\Service;

use AppBundle\Entity\Page;
use Facebook\FacebookResponse;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16.06.2017
 * Time: 15:54
 */

/**
 * Class Facebook
 * @package AppBundle\Service
 */
class Facebook
{
    /**
     * @var FacebookConnection
     */
    private $connection;

    /**
     * @var FacebookResponse $response
     */
    private $response;

    /**
     * Facebook constructor.
     * @param FacebookConnection $connection
     */
    public function __construct(FacebookConnection $connection)
    {
        $this->connection = $connection;
    }

    public function getData($id, $fields = null)
    {
        if ($fields) {
            $query = '?' . http_build_query(['fields' => implode(',', $fields)]);
        } else {
            $query = '';
        }

        $this->response = $this->connection->getFacebook()->get('/' . $id . $query);
        return $this;
    }

    public function getReactionCountForPost($id, $reaction)
    {
        $fields = ['reactions.type(' . strtoupper($reaction) . ').limit(0).summary(total_count)'];

        $data = $this->getData($id, $fields)->getResult();

        return $data['reactions']['summary']['total_count'];

    }

    public function getReactionsForPostCount($id)
    {
        $reactions = ['NONE','LIKE', 'LOVE', 'WOW', 'HAHA', 'SAD', 'ANGRY', 'THANKFUL', 'PRIDE'];

        foreach ($reactions as $index => $reaction) {
            $count = $this->getReactionCountForPost($id, $reaction);
            $reactions[$index] = $count;
        }

        return $reactions;
    }

    public function countPosts(Page $page)
    {
        $after = null;
        $posts = 0;

        do {

            if ($after) {
                $data = $this->getPageData($page, ['posts.limit(50){id}'], $after)->getResult();
            } else {
                $data = $this->getPageData($page, ['posts.limit(50){id}'])->getResult();
            }

            if (!isset($data['posts'])) {
                break;
            }

            $posts += count($data['posts']['data']);

            if (isset($data['posts']['paging'])) {
                if (isset($data['posts']['paging']['cursors'])) {
                    if (isset($data['posts']['paging']['cursors']['after'])) {
                        $after = $data['posts']['paging']['cursors']['after'];
                    } else {
                        $after = null;
                    }
                } else {
                    $after = null;
                }

            } else {
                $after = null;
            }

        } while ($after != null);

        return $posts;
    }

    /**
     * @return array|bool
     */
    public function getResult()
    {
        if ($this->response !== null) {
            return $this->response->getDecodedBody();
        }

        return false;
    }

    /**
     * @param Page $page
     * @param array|null $fields
     * @return $this
     */
    public function getPageData(Page $page, array $fields = null, $after = null)
    {
        $parameters = ['locale' => $this->connection->getLocale()];
        if ($fields) {
            $parameters['fields'] = implode(',', $fields);
        }

        if ($after) {
            $parameters['after'] = $after;
        }

        $query = http_build_query($parameters);

        $this->response = $this->connection->getFacebook()->get('/' . $page->getFacebookID() . '?' . $query);

        return $this;

    }

    /**
     * @param $query
     * @param string $type
     * @param int $limit
     * @param array|null $fields
     * @param null $after
     * @return $this
     */
    public function search($query, $type = 'page', $limit = 5, array $fields = null, $after = null)
    {
        $parameters = ['q' => $query, 'type' => $type, 'limit' => $limit, 'locale' => $this->connection->getLocale()];

        if ($after) {
            $parameters['after'] = $after;
        }

        if ($fields) {
            $parameters['fields'] = implode(',', $fields);
        }

        $query = http_build_query($parameters);

        $this->response = $this->connection->getFacebook()->get('/search?' . $query);

        return $this;
    }

    /**
     * @param $url
     * @return bool|mixed
     */
    public function getPageIdByUrl($url)
    {
        $this->response = $this->connection->getFacebook()->get('/' . $url);
        $data = $this->getResult();
        if (isset($data['id'])) {
            return $data['id'];
        }

        return false;

    }

    public static function getCountryList()
    {
        $file = __DIR__ . '/../../../app/Resources/translation/countries.json';
        $countries = json_decode(file_get_contents($file), true);
        return $countries;
    }

    public static function translateCountryNameToGerman($country)
    {
        $countries = Facebook::getCountryList();
        if (isset($countries[$country])) {
            return $countries[$country];
        }

        return $country;
    }

}