<?php

namespace AppBundle\Service;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Search;
use Doctrine\Common\Persistence\ObjectManager;

class SearchHelper
{
    /**
     * @var ObjectManager
     */
    private $em;
    private $maxSearch;

    public function __construct(ObjectManager $em, $maxSearch)
    {

        $this->em = $em;
        $this->maxSearch = $maxSearch;
    }

    public function countPagesForSearchJobAndUser(Search $search, Customer $customer)
    {
        $pageRepo = $this->em->getRepository('AppBundle:Page');

        $pageCount = count($pageRepo->findBySearch($search));

        return $pageCount;
    }

    public function countPagesInSearch(Search $search)
    {
        $pageRepo = $this->em->getRepository('AppBundle:Page');

        $pageCount = $pageRepo->countBySearchID($search);

        return $pageCount;
    }
    public function countNewPagesInSearch(Search $search)
    {
        $pageRepo = $this->em->getRepository('AppBundle:Page');

        $pageCount = $pageRepo->countNewBySearchID($search);

        return $pageCount;
    }

    public function getMaxSearchesPerUser()
    {
        return $this->maxSearch;
    }



}