<?php

namespace AppBundle\Service;

class CityApiService
{
    public function isDEZip($zip)
    {
        $result = json_decode(file_get_contents('https://api.appworkx.de/api/?c=de&zip=' . $zip), true);
        if (isset($result['yes'])) {
            return true;
        } else {
            return false;
        }
    }

    public function isDECity($city)
    {
        $result = json_decode(file_get_contents('https://api.appworkx.de/api/?c=de&city=' . $city), true);
        if (isset($result['yes'])) {
            return true;
        } else {
            return false;
        }
    }

    public function isATZip($zip)
    {
        $result = json_decode(file_get_contents('https://api.appworkx.de/api/?c=at&zip=' . $zip), true);
        if (isset($result['yes'])) {
            return true;
        } else {
            return false;
        }
    }

    public function isATCity($city)
    {
        $result = json_decode(file_get_contents('https://api.appworkx.de/api/?c=at&city=' . $city), true);
        if (isset($result['yes'])) {
            return true;
        } else {
            return false;
        }
    }

    public function isCHZip($zip)
    {
        $result = json_decode(file_get_contents('https://api.appworkx.de/api/?c=ch&zip=' . $zip), true);
        if (isset($result['yes'])) {
            return true;
        } else {
            return false;
        }
    }

    public function isCHCity($city)
    {
        $result = json_decode(file_get_contents('https://api.appworkx.de/api/?c=ch&city=' . $city), true);
        if (isset($result['yes'])) {
            return true;
        } else {
            return false;
        }
    }


}