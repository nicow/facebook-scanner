<?php

namespace AppBundle\Service;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Page;
use AppBundle\Entity\PageList;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16.06.2017
 * Time: 17:47
 */


class PageHelper {

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    public function getNewPage($facebookId)
    {
        $page = new Page();
        $page->setFanCount(0);
        $page->setVerified(false);
        $page->setTalkingAbout(0);
        $page->setWereHere(0);
        $page->setFacebookId($facebookId);
        $page->setDisabled(false);
        $page->setScrapeDate((new \DateTime()));
        $page->setPostCount(0);
        $page->setDeleted(false);
        $page->setRead(false);

        return $page;

    }

    /**
     * @param $name
     * @param Customer $customer
     * @return PageList
     */
    public function getNewPageList($name, Customer $customer)
    {
        $list = new PageList();
        $list->setCustomer($customer);
        $list->setName($name);

        return $list;
    }

    /**
     * @param string $id
     * @param array|null $selection
     * @return mixed
     */
    public function countrySelect($id = 'countries', array $selection = [])
    {
        $select = '<select multiple id="' . $id . '" class="form-control">%list%</select>';

        $list = "";
        foreach (Facebook::getCountryList() as $name => $translated) {

            if (in_array($name, $selection)) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }

            $list .= '<option value="' . $name . '" ' . $selected . '>' . $translated . '</option>';
        }

        return str_replace('%list%', $list, $select);
    }

    /**
     * @param Page $page
     * @param array $data
     * @return Page|bool
     */
    public function fillPage(Page $page, array $data, array $country_filter = null, $useWithoutCountry = true, CityApiService $api = null)
    {
        $page->setCountry(null);

        if (!in_array('Austria', $country_filter) && !in_array('Germany', $country_filter) && !in_array('Switzerland', $country_filter) && $useWithoutCountry) {
            return false;
        }

        if (in_array('Austria', $country_filter)) {
            if (isset($data['location']['country'])) {
                if ($data['location']['country'] == 'Austria') {
                    $country = Facebook::translateCountryNameToGerman($data['location']['country']);
                    $page->setCountry($country);
                }
            }
            if (isset($data['location']['zip']) && $page->getCountry() == null) {
                if ($api->isATZip($data['location']['zip'])) {
                    $country = Facebook::translateCountryNameToGerman('Austria');
                    $page->setCountry($country);
                }
            }
            if (isset($data['location']['city']) && $page->getCountry() == null) {
                if ($api->isATCity($data['location']['city'])) {
                    $country = Facebook::translateCountryNameToGerman('Austria');
                    $page->setCountry($country);
                }
            }

            if ($page->getCountry() == null) {
                if (!$useWithoutCountry) {
                    return false;
                }
            }

        }

        if (in_array('Germany', $country_filter)) {
            if (isset($data['location']['country'])) {
                if ($data['location']['country'] == 'Germany') {
                    $country = Facebook::translateCountryNameToGerman($data['location']['country']);
                    $page->setCountry($country);
                }
            }
            if (isset($data['location']['zip']) && $page->getCountry() == null) {
                if ($api->isDEZip($data['location']['zip'])) {
                    $country = Facebook::translateCountryNameToGerman('Germany');
                    $page->setCountry($country);
                }
            }
            if (isset($data['location']['city']) && $page->getCountry() == null) {
                if ($api->isDECity($data['location']['city'])) {
                    $country = Facebook::translateCountryNameToGerman('Germany');
                    $page->setCountry($country);
                }
            }

            if ($page->getCountry() == null) {
                if (!$useWithoutCountry) {
                    return false;
                }
            }

        }


        if (in_array('Switzerland', $country_filter)) {
            if (isset($data['location']['country'])) {
                if ($data['location']['country'] == 'Switzerland') {
                    $country = Facebook::translateCountryNameToGerman($data['location']['country']);
                    $page->setCountry($country);
                }
            }
            if (isset($data['location']['zip']) && $page->getCountry() == null) {
                if ($api->isDEZip($data['location']['zip'])) {
                    $country = Facebook::translateCountryNameToGerman('Switzerland');
                    $page->setCountry($country);
                }
            }
            if (isset($data['location']['city']) && $page->getCountry() == null) {
                if ($api->isDECity($data['location']['city'])) {
                    $country = Facebook::translateCountryNameToGerman('Switzerland');
                    $page->setCountry($country);
                }
            }

            if ($page->getCountry() == null) {
                if (!$useWithoutCountry) {
                    return false;
                }
            }

        }

        if (isset($data['location']['city'])) {
            $page->setCity($data['location']['city']);
        }


        if (isset($data['location']['zip'])) {
            $page->setZip($data['location']['zip']);
        }

        if (isset($data['location']['street'])) {
            $page->setStreet($data['location']['street']);
        }

        if (isset($data['about'])) {

            $page->setAbout(preg_replace('/[\x00-\x1F\x7F]/u', '',utf8_decode($data['about'])));
        }

        if (isset($data['fan_count'])) {
            $page->setFanCount($data['fan_count']);
        }

        if (isset($data['is_verified'])) {
            $page->setVerified((bool)$data['is_verified']);
        }

        if (isset($data['link'])) {
            $page->setLink(preg_replace('/[\x00-\x1F\x7F]/u', '', utf8_decode($data['link'])));
        }

        if (isset($data['talking_about_count'])) {
            $page->setTalkingAbout($data['talking_about_count']);
        }
        if (isset($data['category'])) {
            $page->setCategory($data['category']);
        }

        if (isset($data['displayed_message_response_time'])) {
            $page->setDisplayedMessageResponseTime($data['displayed_message_response_time']);
        }

        if (isset($data['cover'])) {
            $page->setCover($data['cover']['source']);
        }

        if (isset($data['name']) && $data['name'] != null && $data['name'] != '' && preg_replace('/[\x00-\x1F\x7F]/u', '',utf8_decode($data['name'])) != '' && preg_replace('/[\x00-\x1F\x7F]/u', '',utf8_decode($data['name'])) != null) {
            $page->setName(preg_replace('/[\x00-\x1F\x7F]/u', '',utf8_decode($data['name'])));
            $page->setCleanName(preg_replace('/[\x00-\x1F\x7F]/u', '',utf8_decode($data['name'])));
        }

        if (isset($data['phone'])) {
            $page->setPhone($data['phone']);
        }

        if (isset($data['impressum'])) {
            $page->setImpressum($data['impressum']);
        }

        if (isset($data['username'])) {
            $page->setUsername($data['username']);
        }

        if (isset($data['website'])) {
            $page->setWebsite($data['website']);
        }

        if (isset($data['picture'])) {
            $page->setPicture($data['picture']['data']['url']);
        }

        if (isset($data['description'])) {
            $page->setDescription($data['description']);
        }

        if (isset($data['were_here_count'])) {
            $page->setWereHere($data['were_here_count']);
        }

        $page->setFbdeleted(false);

        return $page;
    }

    public function getNewPages(Customer $customer)
    {
        return $this
            ->container
            ->get('doctrine')
            ->getManager()
            ->getRepository('AppBundle:Page')
            ->getSinceCount($customer);

    }

    public function getMaxPagination($customer)
    {
        $maxPagination = $this
            ->container
            ->get('doctrine')
            ->getManager()
            ->getRepository('AppBundle:Customer')
            ->find(['id' => $customer ])
            ->getMaxPagination();

        return $maxPagination;
    }

}