<?php

namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16.06.2017
 * Time: 18:58
 */

/**
 * Class Search
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="SearchRepository")
 * @ORM\Table(name="app_search")
 */
class Search
{
    /**
     * @var int $id
     * @ORM\Column(name="id", type="integer", options={"comment":"Primary key"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var Customer $customer
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $customer;

    /**
     * @var string $term
     * @ORM\Column(name="term", type="string", options={"comment":"The search query"})
     */
    private $term;

    /**
     * @var string $after
     * @ORM\Column(name="after", type="string", nullable=true)
     */
    private $after;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Page", mappedBy="search")
     *
     */
    private $pages;

    /**
     * @var \DateTime $lastComplete
     * @ORM\Column(name="last_complete", type="datetime", nullable=true)
     */
    private $lastComplete;

    /**
     * @var \DateTime $lastServed
     * @ORM\Column(name="last_served", type="datetime", nullable=true)
     */
    private $lastServed;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return string
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * @param string $term
     */
    public function setTerm($term)
    {
        $this->term = $term;
    }

    /**
     * @return string
     */
    public function getAfter()
    {
        return $this->after;
    }

    /**
     * @param string $after
     */
    public function setAfter($after)
    {
        $this->after = $after;
    }

    /**
     * @return mixed
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param mixed $pages
     */
    public function setPages($pages)
    {
        $this->pages = $pages;
    }

    /**
     * @return \DateTime
     */
    public function getLastComplete()
    {
        return $this->lastComplete;
    }

    /**
     * @param \DateTime $lastComplete
     */
    public function setLastComplete($lastComplete)
    {
        $this->lastComplete = $lastComplete;
    }

    /**
     * @return \DateTime
     */
    public function getLastServed()
    {
        return $this->lastServed;
    }

    /**
     * @param \DateTime $lastServed
     */
    public function setLastServed($lastServed)
    {
        $this->lastServed = $lastServed;
    }


}