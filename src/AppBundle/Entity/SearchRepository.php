<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SearchRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class SearchRepository extends EntityRepository
{
    /**
     * @return Search
     */
    public function getNextSearch()
    {
        $qb = $this->createQueryBuilder('s')
            ->orderBy('s.lastServed', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return $qb[0];

    }

}
