<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16.06.2017
 * Time: 18:58
 */

/**
 * Class Search
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="app_page_list")
 */
class PageList
{
    /**
     * @var int $id
     * @ORM\Column(name="id", type="integer", options={"comment":"Primary key"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var Customer $customer
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $customer;


    /**
     * @var string $term
     * @ORM\Column(name="name", type="string", options={"comment":"The name of this list"})
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Page",mappedBy="list" )
     */
    private $pages;


    public function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getPages ()
    {
        if ($this->pages == null) {
            return [];
        }
        return $this->pages;
    }

    public function setPages (array $pages)
    {
        $this->pages = $pages;
    }

}