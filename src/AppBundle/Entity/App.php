<?php
/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 09.08.2017
 * Time: 15:19
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Apps
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="app_app")
 */
class App
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="accesstoken", type="string", length=512)
     */
    protected $accesstoken;
    /**
     * @ORM\Column(name="in_use", type="boolean")
     */
    protected $inUse;
    /**
     * @ORM\Column(name="blocked", type="boolean")
     */
    protected $blocked;

    /**
     * @return mixed
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * @param mixed $blocked
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;
    }

    /**
     * @return mixed
     */
    public function getInUse()
    {
        return $this->inUse;
    }

    /**
     * @param mixed $inUse
     */
    public function setInUse($inUse)
    {
        $this->inUse = $inUse;
    }

    /**
     * @return mixed
     */
    public function getAccesstoken()
    {
        return $this->accesstoken;
    }

    /**
     * @param mixed $accesstoken
     */
    public function setAccesstoken($accesstoken)
    {
        $this->accesstoken = $accesstoken;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}