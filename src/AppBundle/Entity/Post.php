<?php
/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 09.08.2017
 * Time: 15:19
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Apps
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="PostRepository")
 * @ORM\Table(name="app_facebook_post")
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="facebook_id", type="text", nullable=false)
     */
    protected $facebookId;

    /**
     * @ORM\Column(name="message", type="text", length=63206, nullable=true)
     */
    protected $message;

    /**
     * @ORM\Column(name="created_time", type="datetime")
     */
    protected $createdTime;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Page")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $page;




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param mixed $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return utf8_decode($this->message);
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = utf8_encode($message);
    }

    /**
     * @return mixed
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * @param mixed $createdTime
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;
    }

    /**
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }


}