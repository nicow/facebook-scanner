<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_customer")
 */
class Customer extends User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime $pageSince
     * @ORM\Column(name="page_since", type="datetime", nullable=true)
     */
    protected $pageSince;

    /**
     * @var string $searchCountries
     * @ORM\Column(name="search_countries", type="string", nullable=true)
     */
    protected $searchCountries;

    /**
     * @var bool $useResultsWithoutCountry
     * @ORM\Column(name="use_results_without_country", type="boolean", nullable=true)
     */
    protected $useResultsWithoutCountry;

    /**
     * @var string $fullname
     * @ORM\Column(name="fullname", type="string", nullable=true)
     */
    protected $fullname;

    /**
     * @var array $activatedPages
     * @ORM\Column(name="activated_pages", type="string", nullable=true, length=100000)
     *
     */
    protected $activatedPages;

    /**
     * @var int $maxPages
     * @ORM\Column(name="max_pages", type="integer", nullable=true)
     */
    protected $maxPages;

    /**
     * @var int $maxPages
     * @ORM\Column(name="max_pagination", type="integer", nullable=true)
     */
    protected $maxPagination = 20;

    /**
     * @var int $searchJobs
     * @ORM\Column(name="search_jobs", type="integer", nullable=true)
     */
    protected $searchJobs;

    /**
     * @var bool $seeOverview
     * @ORM\Column(name="see_overview", type="boolean", nullable=true)
     */
    protected $seeOverview = true;




    public function __construct()
    {
        parent::__construct();

        if (!$this->maxPages) {
            $this->maxPages = 200;
        }
        if (!$this->maxPagination) {
            $this->maxPagination = 20;
        }
        if (!$this->searchCountries) {
            $this->setSearchCountries(array('Germany'));
        }
        if (!$this->useResultsWithoutCountry) {
            $this->setUseResultsWithoutCountry(false);
        }

    }

    public function getAvatar($size = 80)
    {
        return $this->getGravatar($this->getEmail(), $size);
    }

    private function getGravatar($email, $s = 80, $d = 'mm', $r = 'g')
    {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($email)));
        $url .= "?s=$s&d=$d&r=$r";
        return $url;
    }

    /**
     * @param string $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @return \DateTime
     */
    public function getPageSince()
    {
        return $this->pageSince;
    }

    /**
     * @param \DateTime $pageSince
     */
    public function setPageSince($pageSince)
    {
        $this->pageSince = $pageSince;
    }

    /**
     * @return array
     */
    public function getSearchCountries()
    {
        if ($this->searchCountries == null) {
            return unserialize(serialize([]));
        }
        return unserialize($this->searchCountries);
    }

    /**
     * @param array $searchCountries
     */
    public function setSearchCountries(array $searchCountries)
    {
        $this->searchCountries = serialize($searchCountries);
    }

    /**
     * @return bool
     */
    public function isUseResultsWithoutCountry()
    {
        if ($this->useResultsWithoutCountry === null) {
            $this->setUseResultsWithoutCountry(true);
        }

        return $this->useResultsWithoutCountry;
    }

    /**
     * @param bool $useResultsWithoutCountry
     */
    public function setUseResultsWithoutCountry($useResultsWithoutCountry)
    {

        $this->useResultsWithoutCountry = $useResultsWithoutCountry;
    }

    /**
     * @return array
     */
    public function getActivatedPages()
    {
        if ($this->activatedPages == null) {
            return unserialize(serialize([]));
        }
        return unserialize($this->activatedPages);
    }

    public function setActivatedPages(array $activatedPages)
    {
        $this->activatedPages = serialize($activatedPages);
    }

    /**
     * @return int
     */
    public function getMaxPages()
    {
        if ($this->maxPages === null) {
            $this->setMaxPages('200');
        }
        return $this->maxPages;
    }

    /**
     * @param int $maxPages
     */
    public function setMaxPages($maxPages)
    {
        $this->maxPages = $maxPages;
    }
    /**
     * @return int
     */
    public function getMaxPagination()
    {
        if ($this->maxPagination === null) {
            $this->setMaxPagination('20');
        }
        return $this->maxPagination;
    }

    /**
     * @param int $maxPagination
     */
    public function setMaxPagination($maxPagination)
    {
        $this->maxPagination = $maxPagination;
    }


    /**
     * @return bool
     */
    public function getSeeOverview()
    {
        if ($this->seeOverview === null) {
            $this->setSeeOverview(true);
        }
        return $this->seeOverview;
    }

    /**
     * @param bool $seeOverview
     */
    public function setSeeOverview($seeOverview)
    {
        $this->seeOverview = $seeOverview;
    }

    /**
     * @return int
     */
    public function getSearchJobs()
    {
        if ($this->searchJobs === null) {
            $this->setSearchJobs(5);
        }

        return $this->searchJobs;
    }

    /**
     * @param int $searchJobs
     */
    public function setSearchJobs($searchJobs)
    {
        $this->searchJobs = $searchJobs;
    }

    /**
     * @param $role
     * @return bool
     */
    public function isGranted($role)
    {
        return in_array($role, $this->getRoles());
    }

}