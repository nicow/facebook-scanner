<?php

namespace AppBundle\Entity;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16.06.2017
 * Time: 17:15
 */
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Page
 * @package AppBundle\Entity
 * @ORM\Table(name="app_facebook_page")
 * @ORM\Entity(repositoryClass="PageRepository")
 *
 * See: https://developers.facebook.com/docs/graph-api/reference/page
 */
class Page
{

    /**
     * @var int $id
     * @ORM\Column(name="id", type="integer", options={"comment":"Primary key"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     * @ORM\Column(name="name", type="text", length=15000, nullable=true, options={"comment":"The name of the Page"})
     * The name of the Page
     */
    private $name;

    /**
     * @var string $cleanName
     * @ORM\Column(name="clean_name", type="text", length=15000, nullable=true, options={"comment":"The name of the Page without special chars"})
     * The name of the Page
     */
    private $cleanName;

    /**
     * @var string $about
     * @ORM\Column(name="about", type="text", nullable=true, options={"comment":"Information about the Page"})
     * Information about the Page
     */
    private $about;

    /**
     * @var PageList $list
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PageList", inversedBy="pages")
     * @ORM\JoinColumn(name="list_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $list;

    /**
     * @var string $category
     * @ORM\Column(name="category", type="string", length=150, nullable=true, options={"comment":"The Page's category. e.g. Product/Service, Computers/Technology"})
     * The Page's category. e.g. Product/Service, Computers/Technology
     */
    private $category;

    /**
     * @var string $description
     * @ORM\Column(name="description", type="text", nullable=true, options={"comment":"The description of the Page"})
     * The description of the Page
     */
    private $description;

    /**
     * @var string $displayedMessageTesponseTime
     * @ORM\Column(name="displayed_message_Response_time", nullable=true, type="string", length=20, options={"comment":"Page estimated message response time displayed to user"})
     * Page estimated message response time displayed to user
     */
    private $displayedMessageResponseTime;

    /**
     * @var int $fanCount
     * @ORM\Column(name="fan_count", type="integer", length=10, options={"comment":"The number of users who like the Page. For Global Pages this is the count for all Pages across the brand."})
     * The number of users who like the Page. For Global Pages this is the count for all Pages across the brand.
     */
    private $fanCount;

    /**
     * @var bool $verified
     * @ORM\Column(name="verified", type="boolean", options={"comment":"Pages with a large number of followers can be manually verified by Facebook as having an authentic identity. This field indicates whether the page is verified by this process"})
     * Pages with a large number of followers can be manually verified by Facebook as having an authentic identity. This field indicates whether the page is verified by this process
     */
    private $verified;

    /**
     * @var string $link
     * @ORM\Column(name="link", type="text", nullable=true, length=30000, options={"comment":"The Page's Facebook URL"})
     * The Page's Facebook URL     *
     */
    private $link;

    /**
     * @var int $talkingAbout
     * @ORM\Column(name="talking_about", type="integer", length=10, options={"comment":"The number of people talking about this Page"})
     * The number of people talking about this Page
     */
    private $talkingAbout;

    /**
     * @var int $wereHere
     * @ORM\Column(name="were_here", type="integer", length=10, options={"comment":"The number of visits to this Page's location. If the Page setting Show map, check-ins and star ratings on the Page (under Page Settings > Page Info > Address) is disabled, then this value will also be disabled"})
     * The number of visits to this Page's location. If the Page setting Show map, check-ins and star ratings on the Page (under Page Settings > Page Info > Address) is disabled, then this value will also be disabled
     */
    private $wereHere;

    /**
     * @var string $city
     * @ORM\Column(name="city", type="string", nullable=true, options={"comment":"The city of this page"})
     */
    private $city;

    /**
     * @var string $street
     * @ORM\Column(name="street", type="text", nullable=true, options={"comment":"The street of this page"})
     */
    private $street;

    /**
     * @var int $postCount
     * @ORM\Column(name="post_count",type="integer",options={"comment":"Number of posts on this page"})
     */
    private $postCount;

    /**
     * @var \DateTime $scrapeDate
     * @ORM\Column(name="scrape_date", type="datetime", options={"comment":"The DateTime this page was scraped"})
     */
    private $scrapeDate;

    /**
     * @var string $country
     * @ORM\Column(name="country", type="string", nullable=true, options={"comment":"The country of this page"})
     */
    private $country;

    /**
     * @var string $zip
     * @ORM\Column(name="zip", type="string", length=200, nullable=true, options={"comment":"The zip code of this page"})
     */
    private $zip;

    /**
     * @var string $impressum
     * @ORM\Column(name="impressum", type="text", nullable=true, options={"comment":"The impressum of this page"})
     */
    private $impressum;

    /**
     * @var string $cover
     * @ORM\Column(name="cover", type="string", length=1500, nullable=true, options={"comment":"The cover image url"})
     */
    private $cover;

    /**
     * @var string $phone
     * @ORM\Column(name="phone", type="string", nullable=true, options={"comment":"The phone number of this page"})
     */
    private $phone;

    /**
     * @var string $username
     * @ORM\Column(name="username", type="string", nullable=true, options={"comment":"The pages username"})
     */
    private $username;

    /**
     * @var string $website
     * @ORM\Column(name="website", type="string", length=512, nullable=true, options={"comment":"The website of this page"})
     */
    private $website;

    /**
     * @var string $picture
     * @ORM\Column(name="picture", type="string", length=1500, nullable=true, options={"comment":"The profile picture of this page"})
     */
    private $picture;

    /**
     * @var Customer $customer
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $customer;

    /**
     * @var string $facebookID
     * @ORM\Column(name="facebook_id", type="string", options={"comment":"The facebook graph id"})
     */
    private $facebookID;

    /**
     * @var bool $disabled
     * @ORM\Column(name="disabled", type="boolean")
     */
    private $disabled;

    /**
     * @var float $interactionrate
     * @ORM\Column(name="interactionrate", type="float", precision=2, nullable=true)
     */
    private $interactionrate;


    /**
     * @var bool $deleted
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Search", inversedBy="pages")
     * @ORM\JoinColumn(name="search_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $search;

    /**
     * @var boolean $read
     * @ORM\Column(name="pageread", type="boolean")
     */
    private $read;

    /**
     * @var boolean $fbdeleted
     * @ORM\Column(name="fbdeleted", type="boolean")
     */
    private $fbdeleted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * @param string $about
     */
    public function setAbout($about)
    {
        $this->about = $about;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getWereHere()
    {
        return $this->wereHere;
    }

    /**
     * @param int $wereHere
     */
    public function setWereHere($wereHere)
    {
        $this->wereHere = $wereHere;
    }

    /**
     * @return int
     */
    public function getTalkingAbout()
    {
        return $this->talkingAbout;
    }

    /**
     * @param int $talkingAbout
     */
    public function setTalkingAbout($talkingAbout)
    {
        $this->talkingAbout = $talkingAbout;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return bool
     */
    public function isVerified()
    {
        return $this->verified;
    }

    /**
     * @param bool $verified
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;
    }

    /**
     * @return int
     */
    public function getFanCount()
    {
        return $this->fanCount;
    }

    /**
     * @param int $fanCount
     */
    public function setFanCount($fanCount)
    {
        $this->fanCount = $fanCount;
    }

    /**
     * @return string
     */
    public function getDisplayedMessageResponseTime()
    {
        return $this->displayedMessageResponseTime;
    }

    /**
     * @param string $displayedMessageResponseTime
     */
    public function setDisplayedMessageResponseTime($displayedMessageResponseTime)
    {
        $this->displayedMessageResponseTime = $displayedMessageResponseTime;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getImpressum()
    {
        return $this->impressum;
    }

    /**
     * @param string $impressum
     */
    public function setImpressum($impressum)
    {
        $this->impressum = $impressum;
    }

    /**
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * @param string $cover
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return string
     */
    public function getFacebookID()
    {
        return $this->facebookID;
    }

    /**
     * @param string $facebookID
     */
    public function setFacebookID($facebookID)
    {
        $this->facebookID = $facebookID;
    }

    /**
     * @return \DateTime
     */
    public function getScrapeDate()
    {
        return $this->scrapeDate;
    }

    /**
     * @param \DateTime $scrapeDate
     */
    public function setScrapeDate($scrapeDate)
    {
        $this->scrapeDate = $scrapeDate;
    }

    /**
     * @return bool
     */
    public function isDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param bool $disabled
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getPostCount()
    {
        return $this->postCount;
    }

    /**
     * @param mixed $postCount
     */
    public function setPostCount($postCount)
    {
        $this->postCount = $postCount;
    }

    /**
     * @return PageList|null
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param $list
     */
    public function setList($list)
    {
        $this->list = $list;
    }

    /**
     * @return string
     */
    public function getCleanName()
    {
        return $this->cleanName;
    }

    /**
     * @param string $cleanName
     */
    public function setCleanName($cleanName)
    {
        $cleanName = preg_replace("/[^A-Za-z0-9 ]/", '', $cleanName);
        $this->cleanName = $cleanName;
    }

    /**
     * @return float
     */
    public function getInteractionrate()
    {
        return $this->interactionrate;
    }

    /**
     * @param float $interactionrate
     */
    public function setInteractionrate($interactionrate)
    {
        $this->interactionrate = $interactionrate;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return mixed
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @param mixed $search
     */
    public function setSearch($search)
    {
        $this->search = $search;
    }

    /**
     * @return bool
     */
    public function isRead()
    {
        return $this->read;
    }

    /**
     * @param bool $read
     */
    public function setRead($read)
    {
        $this->read = $read;
    }

    /**
     * @return bool
     */
    public function isFbdeleted()
    {
        return $this->fbdeleted;
    }

    /**
     * @param bool $fbdeleted
     */
    public function setFbdeleted($fbdeleted)
    {
        $this->fbdeleted = $fbdeleted;
    }


}