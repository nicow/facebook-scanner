<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Page;
use AppBundle\Entity\Search;
use AppBundle\Entity\Customer;
use AppBundle\Service\PageHelper;
use Psr\Log\NullLogger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16.06.2017
 * Time: 17:57
 */

/**
 * Class PageController
 * @package AppBundle\Controller
 * @Route("/page")
 */
class PageController extends Controller
{
    const POST_TYPE_LINK = 'link';
    const POST_TYPE_STATUS = 'status';
    const POST_TYPE_PHOTO = 'photo';
    const POST_TYPE_VIDEO = 'video';
    const POST_TYPE_OFFER = 'offer';
    const POST_TYPE_EVENT = 'event';

    /**
     * @param Request $request
     * @Route("/")
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $lists = $em->getRepository('AppBundle:PageList')->findBy(['customer' => $this->getUser()]);
        $searches = $em->getRepository('AppBundle:Search')->findBy(['customer' => $this->getUser()]);
        $user = $this->getUser();
        $sort = $request->query->get('sort', null);
        $direction = $request->query->get('direction', 'asc');
        $new = null;
        $name = null;
        $category = null;
        $adress = null;
        $about = null;
        $description = null;
        $impressum = null;

        $view = $request->query->getInt('view', null);


        if ($request->query->has('s')) {

            $query = $request->query->get('s', null);

            if ($request->query->get('s') == "") {
                return $this->redirectToRoute('app_page_index');
            }

            if ($query) {

                $query = explode(' ', $query);



                if ($request->query->has('field_name')) {
                    $name = true;
                }
                if ($request->query->has('field_cat')) {
                    $category = true;
                }
                if ($request->query->has('field_adr')) {
                    $adress = true;
                }
                if ($request->query->has('field_about')) {
                    $about = true;
                }
                if ($request->query->has('field_descr')) {
                    $description = true;
                }
                if ($request->query->has('field_impr')) {
                    $impressum = true;
                }

                $pList = $request->query->getInt('pList') ? $request->query->getInt('pList') : NULL;
                if ($pList != null) {
                    $filterList = $em->getRepository("AppBundle:PageList")->findOneBy(['id' => $pList]);
                } else {
                    $filterList = null;
                }

                $pSearch = $request->query->getInt('pSearch') ? $request->query->getInt('pSearch') : NULL;
                if ($pSearch != null) {
                    $filterSearch = $em->getRepository("AppBundle:Search")->findOneBy(['id' => $pSearch]);
                } else {
                    $filterSearch = null;
                }

                if (!$name && !$category && !$adress && !$about && !$description && !$impressum) {
                    return $this->redirectToRoute('app_page_index');
                }

                if ($name || $category || $adress || $about || $description || $impressum) {

                    //$result = $repo->searchAllPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $adress, $about, $description, $impressum, $pList, $pSearch);
                    // $filterCount = count($repo->searchAllPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $adress, $about, $description, $impressum, $pList, $pSearch)->getResult());

                    $paginator = $this->get('knp_paginator');


                    $result = $repo->getFilteredPages($query, $user, $sort, $direction, $name, $category, $adress, $about, $description, $impressum, $pList, $pSearch, $new);


                    $pagination = $paginator->paginate(
                        $result, /* query NOT result */
                        $request->query->getInt('page', 1)/*page number*/,
                        $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
                    );


                    $filterCount = $result->getResult();

                    $filterMinus = false;

                    $filterTerm = implode(' ', $query);
                    $filterTerm = str_replace('%', '', $filterTerm);
                    $filterTerm = trim($filterTerm);


                    return $this->render('page/index.html.twig', [
                        'pagination' => $pagination,
                        'filterCount' => $filterCount,
                        'filterTerm' => $filterTerm,
                        'filter_name' => $name,
                        'filter_category' => $category,
                        'filter_adress' => $adress,
                        'filter_about' => $about,
                        'filter_description' => $description,
                        'filter_impressum' => $impressum,
                        'filter_minus' => $filterMinus,
                        'filterList' => $filterList,
                        'filterSearch' => $filterSearch,
                        'lists' => $lists,
                        'searches' => $searches,
                    ]);

                }
            }
        }


            if ($request->query->get('s') == "" || !$request->query->has('s')) {

                $paginator = $this->get('knp_paginator');



                if ($view == 5) {
                    $pagination = $paginator->paginate(
                        $repo->getCustomerQuery($this->getUser()), /* query NOT result */
                        $request->query->getInt('page', 1)/*page number*/,
                        $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination(),/*limit per page*/
                        array('defaultSortFieldName' => 'p.fanCount', 'defaultSortDirection' => 'desc')
                    );
                } elseif ($view == 6) {
                    $pagination = $paginator->paginate(
                        $repo->getCustomerQuery($this->getUser()), /* query NOT result */
                        $request->query->getInt('page', 1)/*page number*/,
                        $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination(),/*limit per page*/
                        array('defaultSortFieldName' => 'p.interactionrate', 'defaultSortDirection' => 'desc')
                    );
                }  else {
                    $pagination = $paginator->paginate(
                        $repo->getCustomerQuery($this->getUser()), /* query NOT result */
                        $request->query->getInt('page', 1)/*page number*/,
                        $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
                    );
                }

                return $this->render('page/index.html.twig', [
                    'pagination' => $pagination,
                    'lists' => $lists,
                    'searches' => $searches,
                ]);
            }

    }


    /**
     * @param Request $request
     * @param Page $page
     *
     * @Route("/{id}/list", options={"expose"=true})
     * @return JsonResponse|RedirectResponse
     */
    public function setListAction(Request $request, Page $page)
    {
        if ($page->getCustomer() == $this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository('AppBundle:PageList');
            $list = $request->query->getInt('list', null);
            if ($list) {
                $list = $repo->findOneBy(['id' => $request->query->getInt('list', $list)]);
                if ($list->getCustomer() == $page->getCustomer()) {
                    $page->setList($list);
                    $em->flush();
                    return new JsonResponse(['status' => 'ok', 'list_name' => $list->getName()]);
                }
            } else {
                $page->setList(null);
                $em->flush();
                if ($request->query->has('json')) {
                    return new JsonResponse(['status' => 'ok', 'list_name' => false]);
                } else {
                    return $this->redirectToRoute('app_list_list');
                }
            }
            return new JsonResponse(['error' => 'Sie können zu dieser Liste keine Seite hinzufügen.']);
        }

        return new JsonResponse(['error' => 'Sie können diese Seite nicht bearbeiten.']);
    }

    /**
     * @param Request $request
     * @param Page $page
     * @Route("/{id}")
     * @return Response
     */
    public function showAction(Request $request, Page $page)
    {
        if ($page->getCustomer() != $this->getUser()) {
            return $this->redirectToRoute('app_page_index');
        }

        $facebook = $this->get('app.facebook');

        $posts = $facebook->getPageData($page, ['posts.limit(10){type,caption,likes,shares,permalink_url,description,created_time,message,id,link,object_id}',])->getResult();
        if (isset($posts['posts'])) {
            foreach ($posts['posts']['data'] as $id => $post) {

                if (isset($post['object_id']) && $post['type'] == self::POST_TYPE_PHOTO) {
                    $data = $facebook->getData($post['object_id'], ['images,name'])->getResult();

                    if (isset($data['name'])) {
                        $post['message'] = $data['name'];
                    }
                    $post['object'] = end($data['images'])['source'];
                }

                if (isset($post['object_id']) && $post['type'] == self::POST_TYPE_VIDEO) {
                    $data = $facebook->getData($post['object_id'], ['description,format'])->getResult();

                    if (isset($data['description'])) {
                        $post['message'] = $data['description'];
                    }

                    if (count($data['format']) > 1) {
                        $post['object'] = $data['format'][1]['embed_html'];
                    } else {
                        $post['object'] = $data['format'][0]['embed_html'];
                    }
                }

                if (!isset($post['message'])) {
                    $post['message'] = "";
                }

                $post['type'] = $this->translatePostType($post['type']);
                if ($post['type'] == false) {
                    continue;
                }

                $posts['posts']['data'][$id] = $post;
            }
        } else {
            $posts = [
                "posts" => ["data" => []]
            ];
        }

        $lists = $this->getDoctrine()->getManager()->getRepository('AppBundle:PageList')->findBy(['customer' => $this->getUser()]);

        return $this->render('page/show.html.twig', ['page' => $page, 'lists' => $lists, 'posts' => $posts['posts']['data']]);
    }

    private function translatePostType($type)
    {
        switch ($type) {
            case self::POST_TYPE_PHOTO:
                return "Foto";
            case self::POST_TYPE_STATUS:
                return "Status Post";
            case self::POST_TYPE_VIDEO:
                return "Video";
            default:
                return false;
        }
    }

    /**
     * @param Page $page
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/page/toggle", name="app_page_toggledisabledstate", options={"expose"=true})
     *
     */
    public function toggleDisabledStateAction(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $searchRepo = $em->getRepository('AppBundle:Search');

        if ($request->request->has('selected')) {

            $selectedPages = $request->request->get('selected');

            //first line is empty for some reason and therefore deleted
            if ($selectedPages[0] == "") {
                unset($selectedPages[0]);
            }

            foreach ($selectedPages as $page_id) {

                /**
                 * @var Page $page
                 */
                $page = $repo->findOneBy(['id' => $page_id, 'customer' => $user]);

                $page->setDisabled(true);
                $page->setRead(true);
            }

            $em->flush();

            return new JsonResponse(['status' => 'ok']);

        } elseif ($request->query->has('pSearch')) {

            $search = $request->query->getInt('pSearch');

            $search = $searchRepo->findBy(['id' => $search]);

            $archived = $repo->findBySearch($search);


            foreach ($archived as $page) {
                $page->setDisabled(true);
                $page->setRead(true);
            }

            $em->flush();

            return $this->redirectToRoute('app_page_index');
        }


        if (strpos($request->headers->get('referer'), 'new') !== false) {
            return $this->redirectToRoute('app_page_newpagesindex');
        }

        return $this->redirectToRoute('app_page_index');
    }

    /**
     * @param Request $request
     * @Route("/api/top10.json", name="app_page_gettop5")
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function getTop10Action(Request $request)
    {
        //now gets top10 pages for different sortings

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');

        /**
         * @var Customer $user
         */
        $user = $this->getUser();

        //4 kinds of sortings for 4 different views
        $by = $request->query->get('by', 'fanCount');
        $byInter = $request->query->get('by', 'interactionrate');
        $byWereHere = $request->query->get('by', 'wereHere');
        $byTalking = $request->query->get('by', 'talkingAbout');


        //filter is now list_id
        $filter = $request->query->getInt('filter');

        $list = $em->getRepository('AppBundle:PageList')->findOneBy(['id' => $filter]);

        //check which lists the user has chosen
        $list_jobs = $user->getActivatedPages();

        //check if the user wants to see an overview
        $overview = $user->getSeeOverview();

        if ($overview == true) {
            $overviewLikes = $repo->getTop10Overview($by, $this->getUser());
            $overviewInter = $repo->getTop10Overview($byInter, $this->getUser());
            $overviewWereHere = $repo->getTop10Overview($byWereHere, $this->getUser());
            $overviewTalking = $repo->getTop10Overview($byTalking, $this->getUser());
        } else {
            $overviewLikes = null;
            $overviewInter = null;
            $overviewWereHere = null;
            $overviewTalking = null;
        }

        //determines the sorting (set in template)
        $display = $request->query->getInt('display');

        $top5 = $repo->getTop10ByList($by, $this->getUser(), $filter);
        $top5Inter = $repo->getTop10ByList($byInter, $this->getUser(), $filter);
        $top5WereHere = $repo->getTop10ByList($byWereHere, $this->getUser(), $filter);
        $top5Talking = $repo->getTop10ByList($byTalking, $this->getUser(), $filter);

        $serializer = $this->get('serializer');

        if ($request->query->has('template')) {

            return $this->render('page/top5Table.html.twig', [
                'items' => $top5,
                'itemsInter' => $top5Inter,
                'itemsWereHere' => $top5WereHere,
                'itemsTalking' => $top5Talking,
                'list' => $list,
                'listJobs' => $list_jobs,
                'display' => $display,
                'overviewLikes' => $overviewLikes,
                'overviewInter' => $overviewInter,
                'overviewWereHere' => $overviewWereHere,
                'overviewTalking' => $overviewTalking,
                ]);
        }


        return new JsonResponse(json_decode($serializer->serialize($top5, 'json')));
    }


    /**
     *
     *
     * @Route("/import/start", options={"expose"=true})
     */
    public function importAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Search');

        $count = 0;
        $tomuch = 0;
        if ($request->files->has('import')) {
            /**
             * @var UploadedFile $importCSV
             */
            $importCSV = $request->files->get('import');
            $content = file_get_contents($importCSV->getPath() . DIRECTORY_SEPARATOR . $importCSV->getFilename());

            $lines = explode(PHP_EOL, $content);
            foreach ($lines as $line) {

                $parts = explode(';', $line);

                $searchTerm = implode(' ', $parts);

                $searchTerm = trim($searchTerm);

                if ($searchTerm = "") {
                    continue;
                }

                if (!$repository->findOneBy(['term' => $searchTerm])) {
                    $search = new Search();
                    $search->setCustomer($this->getUser());
                    $search->setTerm($searchTerm);

                    if (count($repository->findAll()) < $this->getParameter('search_max_job_per_customer')) {
                        $em->persist($search);
                        $em->flush();
                        $count++;
                    } else {
                        $tomuch++;
                    }
                }
            }
            return new JsonResponse(['status' => 'ok', 'count' => $count, 'failed' => $tomuch]);
        }
        return $this->render('import/import.html.twig');
    }

    /**
     *
     * @Route("/archive/start")
     */
    public function archiveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $user = $this->getUser();
        $disabledPages = $repo->findByDisabledQuery($user);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $disabledPages, /* query NOT result */
            $request->query->getInt('page', 1),
            $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
        );

        return $this->render('page/archive.html.twig', ['pagination' => $pagination]);
    }

    /**
     * @Route("/archive/delete", options={"expose"=true})
     */
    public function deleteArchiveAction(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');


        if ($request->request->has('selected')) {

            $selectedPages = $request->request->get('selected');

            //first line is empty for some reason and therefore deleted
            if ($selectedPages[0] == "") {
                unset($selectedPages[0]);
            }

            foreach ($selectedPages as $page_id) {

                /**
                 * @var Page $page
                 */
                $page = $repo->findOneBy(['id' => $page_id, 'customer' => $user]);

                $page->setDeleted(true);
                $page->setRead(true);
            }

            $em->flush();

            return new JsonResponse(['status' => 'ok']);

        }


        $disabledPages = $repo->findByDisabledQuery($this->getUser())->getResult();
        /**
         * @var Page $page
         */
        foreach ($disabledPages as $page) {
            $page->setDeleted(true);
        }
        $em->flush();

        return $this->render('page/archive.html.twig');

    }

    /**
     * @param Request $request
     * @Route("/post/reactions", options={"expose"=true})
     * @return JsonResponse
     */
    public function getReactions(Request $request)
    {
        $post = $request->query->get('post', null);
        if (!$post) {
            return new JsonResponse(['error'], 200);
        }

        $reactions = $this->get('app.facebook')->getReactionsForPostCount($post);

        return new JsonResponse($reactions);
    }

    /**
     * @param Request $request
     * @param Page $page
     * @return JsonResponse
     * @Route("/{id}/rate")
     */
    public function getRateAction(Request $request, Page $page)
    {
        if ($page->getInteractionrate()) {
            return new JsonResponse(["rate" => $page->getInteractionrate()]);
        }
        $facebook = $this->get('app.facebook');

        $base = $request->query->getInt('base', 20);
        if ($base > 40) {
            $base = 20;
        }

        if ($base <= 0) {
            $base = 20;
        }

        $posts = $facebook->getPageData($page, ['posts.limit(' . $base . ')'])->getResult()['posts']['data'];

        $total_count = 0;

        $likes = $facebook->getPageData($page, ['fan_count'])->getResult()['fan_count'];


        foreach ($posts as $post) {
            foreach ($facebook->getReactionsForPostCount($post['id']) as $id => $count) {
                $total_count = $total_count + $count;
            }
        }

        $rate = ($total_count / count($posts) / $likes) * 100;

        $page->setInteractionrate($rate);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(['rate' => $rate, 'reactions' => $total_count, 'posts' => count($posts)]);
    }

    /**
     *
     * @Route("/new/start", options={"expose"=true})
     */
    public function newPagesIndexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $pageRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Page');
        $listRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:PageList');
        $searches = $em->getRepository('AppBundle:Search')->findBy(['customer' => $this->getUser()]);

        /**
         * @var Customer $user
         */
        $user = $this->getUser();

        $since = $user->getPageSince();

        if (!$since) {
            $since = new \DateTime();
        }
        $pSearch = $request->query->getInt('pSearch') ? $request->query->getInt('pSearch') : NULL;
        if ($pSearch != null) {
            $filterSearch = $em->getRepository("AppBundle:Search")->findOneBy(['id' => $pSearch]);
        } else {
            $filterSearch = null;
        }

        $sinceList = $pageRepo->getSinceList($this->getUser());

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $sinceList, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
        );

        return $this->render('page/new.html.twig', [
            'fanpages' => [
                'since' => $pageRepo->getSinceCount($this->getUser())],
            'pagination' => $pagination,
            'lists' => $listRepo->findBy(['customer' => $this->getUser()]),
            'searches' => $searches,
            'filterSearch' => $filterSearch

        ]);
    }

    /**
     *
     * @param Request $request
     * @Route("/new/filtered", options={"expose"=true})
     */
    public function newPagesFilterAction(Request $request)
    {
        $query = $request->query->get('s', null);
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $pageRepo = $em->getRepository('AppBundle:Page');
        $searches = $em->getRepository('AppBundle:Search')->findBy(['customer' => $user]);
        $sort = $request->query->get('sort', null);
        $direction = $request->query->get('direction', 'asc');

        if (!$query) {
            return $this->redirectToRoute('app_page_newpagesindex');
        }

        if ($query) {

            $query = explode(' ', $query);

            $name = null;
            $category = null;
            $adress = null;
            $about = null;
            $description = null;
            $impressum = null;

            if ($request->query->has('field_name')) {
                $name = true;
            }
            if ($request->query->has('field_cat')) {
                $category = true;
            }

            if ($request->query->has('field_adr')) {
                $adress = true;
            }
            if ($request->query->has('field_about')) {
                $about = true;
            }
            if ($request->query->has('field_descr')) {
                $description = true;
            }
            if ($request->query->has('field_impr')) {
                $impressum = true;
            }

            $new = true;

            $pList = null;

            $pSearch = $request->query->getInt('pSearch') ? $request->query->getInt('pSearch') : NULL;
            if ($pSearch != null) {
                $filterSearch = $em->getRepository("AppBundle:Search")->findOneBy(['id' => $pSearch]);
            } else {
                $filterSearch = null;
            }

            if ($name || $category || $adress || $about || $description || $impressum) {

                //$count = count($pageRepo->searchNewPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $adress, $about, $description, $impressum)->getResult());
                //$results = $pageRepo->searchNewPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $adress, $about, $description, $impressum);

                $count = count($pageRepo->getFilteredPages($query, $user, $sort, $direction, $name, $category, $adress, $about, $description, $impressum, $pList, $pSearch, $new)->getResult());
                $results = $pageRepo->getFilteredPages($query, $user, $sort, $direction, $name, $category, $adress, $about, $description, $impressum, $pList, $pSearch, $new);

                $paginator = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                    $results, /* query NOT result */
                    $request->query->getInt('page', 1)/*page number*/,
                    $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
                );

                $lists = $this->getDoctrine()->getManager()->getRepository('AppBundle:PageList')->findBy(['customer' => $this->getUser()]);


                $filterMinus = false;

                $query = implode(' ', $query);
                $query = str_replace('%', '', $query);
                $query = trim($query);

                return $this->render('page/new.html.twig', [
                    'pagination' => $pagination,
                    'name' => $query,
                    'count' => $count,
                    'lists' => $lists,
                    'filter_name' => $name,
                    'filter_category' => $category,
                    'filterSearch' => $filterSearch,
                    'searches' => $searches,
                    'filter_adress' => $adress,
                    'filter_about' => $about,
                    'filter_description' => $description,
                    'filter_impressum' => $impressum,
                    'filter_minus' => $filterMinus

                ]);
            } else {
                return $this->redirectToRoute('app_page_newpagesindex');
            }
        }
        return $this->redirectToRoute('app_page_newpagesindex');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/new/end", options={"expose"=true})
     */
    public function endNewPagesAction(Request $request)
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');

        if ($request->request->has('selected')) {

            $selectedPages = $request->request->get('selected');

            //first line is empty and therefore deleted
            unset($selectedPages[0]);

            /**
             * @var Page $page
             */
            foreach ($selectedPages as $page_id) {

                $page = $repo->findOneBy(['id' => $page_id, 'customer' => $user]);

                $page->setRead(true);
            }

            if ($request->request->has('list')) {

                $list = $em
                    ->getRepository('AppBundle:PageList')
                    ->findOneBy(['id' => $request->request->get('list'), 'customer' => $user]);

                foreach ($selectedPages as $page_id) {

                    $page = $repo->findOneBy(['id' => $page_id, 'customer' => $user]);

                    $page->setList($list);
                }
            }

            $em->flush();

            return new JsonResponse(['status' => 'ok']);
        }

           return $this->redirectToRoute('app_page_newpagesindex');
        }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/new/tolist", options={"expose"=true})
     */
    public function toListAction(Request $request)
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');

        if ($request->request->has('selected')) {

            $selectedPages = $request->request->get('selected');

            //first line is empty and therefore deleted
            unset($selectedPages[0]);

            /**
             * @var Page $page
             */
            foreach ($selectedPages as $page_id) {

                $page = $repo->findOneBy(['id' => $page_id, 'customer' => $user]);

                $page->setRead(true);
            }

            if ($request->request->has('list')) {

                $list = $em
                    ->getRepository('AppBundle:PageList')
                    ->findOneBy(['id' => $request->request->get('list'), 'customer' => $user]);


                /**
                 * @var Page $page
                 */
                foreach ($selectedPages as $page_id) {

                    $page = $repo->findOneBy(['id' => $page_id, 'customer' => $user]);

                    $page->setList($list);
                }

            }

            $em->flush();

            return new JsonResponse(['status' => 'ok']);

        } else {
            return new JsonResponse(['status' => 'error']);
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/new/endlist", options={"expose"=true})
     */
    public function endListAction(Request $request)
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');

        if ($request->request->has('selected')) {

            $selectedPages = $request->request->get('selected');

            //first line is empty and therefore deleted
            unset($selectedPages[0]);

            /**
             * @var Page $page
             */
            foreach ($selectedPages as $page_id) {

                $page = $repo->findOneBy(['id' => $page_id, 'customer' => $user]);

                $page->setList(null);

                $page->setRead(true);

            }

            $em->flush();

            return new JsonResponse(['status' => 'ok']);

        } else {
            return new JsonResponse(['status' => 'error']);
        }


    }


    /**
     * @param Page $page
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/page/archiveSearch", name="app_page_archiveSearch", options={"expose" = true})
     *
     */
    public function archiveSearchAction(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $searchRepo = $em->getRepository('AppBundle:Search');

        if ($request->query->has('pSearch')) {

            $searchID = $request->query->getInt('pSearch');

            if ($searchRepo->findOneBy(['id' => $searchID, 'customer' => $user])) {

                if ($request->query->has('N')) {

                    $archived = $repo->findBy([
                        'search' => $searchID,
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => false,
                    ]);

                } else {

                    $archived = $repo->findBy([
                        'search' => $searchID,
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => true,
                    ]);
                }

                /**
                 * @var Page $page
                 */
                foreach ($archived as $page) {
                    $page->setDisabled(true);
                    $page->setRead(true);
                }
                $em->flush();

            } else {

                if ($request->query->has('N')) {

                    $archived = $repo->findBy([
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => false,
                    ]);

                } else {

                    $archived = $repo->findBy([
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => true,
                    ]);
                }

                /**
                 * @var Page $page
                 */
                foreach ($archived as $page) {
                    $page->setDisabled(true);
                    $page->setRead(true);
                }
                $em->flush();

                return $this->redirectToRoute('app_page_archive');

            }
            return $this->redirectToRoute('app_page_newpagesindex');
        }
        return $this->redirectToRoute('app_page_newpagesindex');
    }

    /**
     * @param Page $page
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/page/deleteSearch", name="app_page_deleteSearch", options={"expose" = true})
     *
     */
    public function deleteSearchAction(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $searchRepo = $em->getRepository('AppBundle:Search');

        if ($request->query->has('pSearch')) {

            $searchID = $request->query->getInt('pSearch');

            if ($searchRepo->findOneBy(['id' => $searchID, 'customer' => $user])) {

                if ($request->query->has('N')) {
                    $delete = $repo->findBy([
                        'search' => $searchID,
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => false,
                    ]);

                } else {
                    $delete = $repo->findBy([
                        'search' => $searchID,
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => true,
                    ]);
                }

                foreach ($delete as $page) {
                    /**
                     * @var Page $page
                     */
                    $page->setDeleted(true);
                    $page->setRead(true);

                }
                $em->flush();
            }

        } else {

            if ($request->query->has('N')) {
                $delete = $repo->findBy([
                    'customer' => $user,
                    'deleted' => false,
                    'disabled' => false,
                    'read' => false,
                ]);

            } else {
                $delete = $repo->findBy([
                    'customer' => $user,
                    'deleted' => false,
                    'disabled' => false,
                    'read' => true,
                ]);
            }

            foreach ($delete as $page) {
                /**
                 * @var Page $page
                 */
                $page->setDeleted(true);
                $page->setRead(true);

            }

            $em->flush();
        }
        return $this->redirectToRoute('app_page_index');

    }

    /**
     * @param Page $page
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/page/readSearch", name="app_page_readSearch", options={"expose" = true})
     *
     */
    public function readSearchAction(Request $request)
    {

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $searchRepo = $em->getRepository('AppBundle:Search');

        if ($request->query->has('pSearch')) {

            $searchID = $request->query->getInt('pSearch');

            if ($searchRepo->findOneBy(['id' => $searchID, 'customer' => $user])) {


                if ($request->query->has('N')) {
                    $read = $repo->findBy([
                        'search' => $searchID,
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => false,
                    ]);

                } else {

                    $read = $repo->findBy([
                        'search' => $searchID,
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => true,
                    ]);
                }

                foreach ($read as $page) {
                    /**
                     * @var Page $page
                     */
                    $page->setRead(true);
                }

                $em->flush();
            }
        } else {


            if ($request->query->has('N')) {
                $read = $repo->findBy([
                    'customer' => $user,
                    'deleted' => false,
                    'disabled' => false,
                    'read' => false,
                ]);

            } else {

                $read = $repo->findBy([
                    'customer' => $user,
                    'deleted' => false,
                    'disabled' => false,
                    'read' => true,
                ]);
            }

            foreach ($read as $page) {
                /**
                 * @var Page $page
                 */
                $page->setRead(true);
            }

            $em->flush();

        }

        return $this->redirectToRoute('app_page_index');

    }

    /**
     * @param Page $page
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/page/searchToList", name="app_page_searchToList", options={"expose" = true})
     *
     */
    public function searchToListAction(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $searchRepo = $em->getRepository('AppBundle:Search');
        $listRepo = $em->getRepository('AppBundle:PageList');

        if ($request->request->has('pSearch') && $request->request->has('pList')) {

            $searchID = $request->request->getInt('pSearch');
            $listID = $request->request->getInt('pList');

            if ($searchRepo->findOneBy(['id' => $searchID, 'customer' => $user])) {

                if ($listRepo->findOneBy(['id' => $listID, 'customer' => $user])) {

                    if ($request->request->has('N')) {
                        $toList = $repo->findBy([
                            'search' => $searchID,
                            'customer' => $user,
                            'deleted' => false,
                            'disabled' => false,
                            'read' => false,
                        ]);

                    } else {

                        $toList = $repo->findBy([
                            'search' => $searchID,
                            'customer' => $user,
                            'deleted' => false,
                            'disabled' => false,
                            'read' => true,
                        ]);
                    }

                    foreach ($toList as $page) {
                        /**
                         * @var Page $page
                         */
                        $page->setList($listRepo->findOneBy(['id' => $listID, 'customer' => $user]));
                        $page->setRead(true);
                      }


                    $em->flush();
                    return new JsonResponse(['status' => 'ok']);
                }
            }

        }
        else {

            $listID = $request->request->getInt('pList');

            if ($listRepo->findOneBy(['id' => $listID, 'customer' => $user])) {

                if ($request->request->has('N')) {
                    $toList = $repo->findBy([
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => false,
                    ]);

                } else {
                    $toList = $repo->findBy([
                        'customer' => $user,
                        'deleted' => false,
                        'disabled' => false,
                        'read' => true,
                    ]);
                }

                foreach ($toList as $page) {
                    /**
                     * @var Page $page
                     */
                    $page->setList($listRepo->findOneBy(['id' => $listID, 'customer' => $user]));
                    $page->setRead(true);
                }

                $em->flush();

                return new JsonResponse(['status' => 'ok']);
            }

        }

            return new JsonResponse(['status' => 'error']);

        }

    /**
     * @param Page $page
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @Route("/page/enable", name="app_page_enable", options={"expose" = true})
     *
     */
    public function enableAllAction(Request $request)
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $archived = $em->getRepository('AppBundle:Page')->createQueryBuilder('p')
            ->select('p')
            ->where('p.disabled = true')
            ->andWhere('p.customer = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();

        /**
         * @var Page $page
         */
        foreach ($archived as $page) {
            $page->setDisabled(false);
        }

        $em->flush();

        return new JsonResponse(['status' => 'ok']);
    }


}

