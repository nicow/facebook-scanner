<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PageList;
use AppBundle\Entity\Page;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 18.06.2017
 * Time: 15:42
 */

/**
 * Class ListController
 * @package AppBundle\Controller
 * @Route("/page/list")
 */
class ListController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/")
     */
    public function listAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:PageList');

        $lists = $repo->findBy(['customer' => $this->getUser()]);

        return $this->render("list/index.html.twig", ['lists' => $lists]);
    }

    /**
     * @param Request $request
     * @param PageList $list
     * @Route("/{id}/delete", options={"expose"=true})
     * @return JsonResponse
     */
    public function deleteAction(Request $request, PageList $list)
    {

        if ($list->getCustomer() == $this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($list);
            $em->flush();
        }

        return new JsonResponse(['status' => 'ok']);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Method("POST")
     * @Route("/new")
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repo = $em->getRepository('AppBundle:PageList');
        if (count($repo->findBy(['customer' => $this->getUser()])) >= $this->getParameter('list_max_per_customer')) {
            return new JsonResponse(['error' => 'Sie können maximal ' . $this->getParameter('list_max_per_customer') . ' Listen anlegen.']);
        }

        $name = $request->request->get('name');
        $list = $this->get('app.helper.page')->getNewPageList($name, $this->getUser());

        $em->persist($list);
        $em->flush();

        return new JsonResponse(['id' => $list->getId(), 'name' => $list->getName()]);
    }


    /**
     * @param Request $request
     * @param PageList $list
     * @Route("/{id}", name="app_list_showlist", options={"expose"=true})
     * @return Response
     */

    public function showListAction(Request $request, PageList $list)
    {
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $view = $request->query->getInt('view', null);

        $lists = $repo
            ->createQueryBuilder('p')
            ->where('p.list = :list')
            ->andWhere('p.read = true')
            ->andWhere('p.disabled = false')
            ->andWhere('p.customer = :user')
            ->setParameter('list',$list)
            ->setParameter('user',$user)
            ->getQuery();

        $searches = $em->getRepository('AppBundle:Search')->findBy(['customer' => $user]);

        $name = $list->getName();
        $id = $list->getId();

        $paginator = $this->get('knp_paginator');

        if ($view == 1) {

            $pagination = $paginator->paginate(
                $lists,
                $request->query->getInt('page', 1)/*page number*/,
                $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination(),/*limit per page*/
                array('defaultSortFieldName' => 'p.fanCount', 'defaultSortDirection' => 'desc')
            );
        } elseif ($view == 2) {

            $pagination = $paginator->paginate(
                $lists,
                $request->query->getInt('page', 1)/*page number*/,
                $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination(),/*limit per page*/
                array('defaultSortFieldName' => 'p.interactionrate', 'defaultSortDirection' => 'desc')
            );
        } else {
        $pagination = $paginator->paginate(
            $lists,
          $request->query->getInt('page', 1)/*page number*/,
            $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
        );
        }

        return $this->render("list/view.html.twig", [
            'lists' => $lists,
            'list' => $list,
            'name' => $name,
            'pagination' => $pagination,
            'id' => $id,
            'searches' => $searches,
        ]);


    }


    /**
     *
     * @param Request $request
     * @Route("/{id}/filtered", options={"expose"=true})
     */
    public function ListFilterAction(Request $request)
    {
        $query = $request->query->get('s', null);
        $listId = $request->query->get('id', null);
        $em = $this->getDoctrine()->getManager();
        $pageRepo = $em->getRepository('AppBundle:Page');
        $listRepo = $em->getRepository('AppBundle:PageList');
        $user = $this->getUser();
        $sort = $request->query->get('sort', null);
        $direction = $request->query->get('direction', 'asc');
        $searches = $em->getRepository('AppBundle:Search')->findBy(['customer' => $user]);
        $view = $request->query->getInt('view', null);

        if (!$query) {
            return $this->redirectToRoute('app_list_list');
        }

        if ($query) {

            $query = explode(' ', $query);

            $name = null;
            $category = null;
            $adress = null;
            $about = null;
            $description = null;
            $impressum = null;

            if ($request->query->has('field_name')) {
                $name = true;
            }
            if ($request->query->has('field_cat')) {
                $category = true;
            }
            if ($request->query->has('field_adr')) {
                $adress = true;
            }
            if ($request->query->has('field_about')) {
                $about = true;
            }
            if ($request->query->has('field_descr')) {
                $description = true;
            }
            if ($request->query->has('field_impr')) {
                $impressum = true;
            }

            if ($name || $category || $adress || $about || $description || $impressum ) {

                $pList = $listId;

                $pSearch = $request->query->getInt('pSearch') ? $request->query->getInt('pSearch') : NULL;
                if ($pSearch != null) {
                    $filterSearch = $em->getRepository("AppBundle:Search")->findOneBy(['id' => $pSearch]);
                } else {
                    $filterSearch = null;
                }

                $new = null;

               // if ($listId) {
               //     $count = count($pageRepo->searchListPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $searchterm, $adress, $about, $description, $impressum, $listRepo->find($listId))->getResult());
                //    $results = $pageRepo->searchListPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $searchterm, $adress, $about, $description, $impressum, $listRepo->find($listId));

               // } else {
               //     $count = count($pageRepo->searchListPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $adress, $about, $description, $impressum, null)->getResult());
               //     $results = $pageRepo->searchListPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $adress, $about, $description, $impressum, null);

               // }
                $count = count($pageRepo->getFilteredPages($query, $user, $sort, $direction, $name, $category, $adress, $about, $description, $impressum, $pList, $pSearch, $new)->getResult());
                $results = $pageRepo->getFilteredPages($query, $user, $sort, $direction, $name, $category, $adress, $about, $description, $impressum, $pList, $pSearch, $new);

                $paginator = $this->get('knp_paginator');


                if ($view == 1) {

                $pagination = $paginator->paginate(
                    $results, /* query NOT result */
                    $request->query->getInt('page', 1)/*page number*/,
                    $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination(),/*limit per page*/
                    array('defaultSortFieldName' => 'p.fanCount', 'defaultSortDirection' => 'desc')
                );
            } elseif ($view == 2) {

                $pagination = $paginator->paginate(
                    $results, /* query NOT result */
                    $request->query->getInt('page', 1)/*page number*/,
                    $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination(),/*limit per page*/
                    array('defaultSortFieldName' => 'p.interactionrate', 'defaultSortDirection' => 'desc')
                );
            } else {

                    $pagination = $paginator->paginate(
                        $results, /* query NOT result */
                        $request->query->getInt('page', 1)/*page number*/,
                        $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
                    );


                    $lists = $this->getDoctrine()->getManager()->getRepository('AppBundle:PageList')->findBy(['customer' => $this->getUser()]);

                    if ($listId) {
                        $meList = $listRepo->find($listId);

                    } elseif ($listId = $request->query->get('listID')) {
                        $meList = $request->query->get('listID');
                        $meList = $listRepo->find($meList);

                    } else {
                        return $this->redirectToRoute('app_list_list');
                    }

                    $query = implode(' ', $query);

                    return $this->render('list/view.html.twig', [
                        'pagination' => $pagination,
                        'count' => $count,
                        'name' => $query,
                        'pages' => $count,
                        'lists' => $lists,
                        'list' => $meList,
                        'filter_name' => $name,
                        'filter_category' => $category,
                        'filter_adress' => $adress,
                        'filter_about' => $about,
                        'filter_description' => $description,
                        'filter_impressum' => $impressum,
                        'id' => $listId,
                        'searches' => $searches,
                        'filterSearch' => $filterSearch,

                    ]);
                }
            } else {
                return $this->redirectToRoute('app_list_list');
            }

        }
        return $this->redirectToRoute('app_list_list');
    }


}

