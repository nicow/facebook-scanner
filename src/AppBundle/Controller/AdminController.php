<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 06.09.17
 * Time: 17:21
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use ColourStream\Bundle\CronBundle\Entity\CronJobResult;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdminController
 * @package AppBundle\Controller
 * @Route("/admin")
 */
class AdminController extends Controller
{

    /**
     * @Route("/", name="admin")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Sie haben keine administrativen Rechte.');

        /**
         * @var Customer $user
         */
        $user = $this->getUser();
        $userID = $user->getId();
        $em = $this->getDoctrine()->getManager();

        $accounts = $em->getRepository('AppBundle:Customer')
            ->createQueryBuilder('acc')
            ->select('acc')
            ->orderBy('acc.roles', 'desc')
            ->getQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $accounts, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
        );


        //count Pages for all accounts
        $allUserPages = 0;
        $countAccounts = $accounts->getResult();
        foreach ($countAccounts as $user) {
            $allUserPages = $allUserPages + $user->getMaxPages();
        }

        //count Searches for all accounts
        $allUserSearches = 0;
        $countAccounts = $accounts->getResult();
        foreach ($countAccounts as $user) {
            $allUserSearches = $allUserSearches + $user->getSearchJobs();
        }

        //count all accounts
        $allUserAccounts = count($em->getRepository('AppBundle:Customer')->findAll());

        return $this->render('admin/index.html.twig', [
            'pagination' => $pagination,
            'accounts' => $accounts,
            'allUserPages' => $allUserPages,
            'allUserSearches' => $allUserSearches,
            'allUserAccounts' => $allUserAccounts,
        ]);

    }

    /**
     * @Route("/update", name="updateLimit", options={"expose"=true})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @return JsonResponse
     */
    public function updateLimitAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Sie haben keine administrativen Rechte.');

        /**
         * @var Customer $user
         */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $accounts = $em->getRepository('AppBundle:Customer')->findAll();

        //kommt über AJAX
        $accID = trim($request->request->getInt('accID'));
        $accSearches = trim($request->request->getInt('accSearches'));
        $accPages = trim($request->request->getInt('accPages'));


        // maximale Fanpages/Suchen pro Installation
        $maxPages = $this->container->getParameter('admin_max_pages');
        $maxSearches = $this->container->getParameter('admin_max_searches');

        /*
         * @var Customer $user
         */
        //zählen, wie viele Seiten die User haben
        $allUserPages = 0;
        foreach ($accounts as $user) {
            $allUserPages = $allUserPages + $user->getMaxPages();
        }
        //zählen, wie viele Suchen die User haben
        $allUserSearches = 0;
        foreach ($accounts as $user) {
            $allUserSearches = $allUserSearches + $user->getSearchJobs();
        }

        $account = $em->getRepository('AppBundle:Customer')->findOneBy(['id' => $accID]);

        if (($allUserPages + ($accPages - $account->getMaxPages())) > $maxPages) {
            $pagesAvail = $maxPages - $allUserPages;
            return new JsonResponse(['limitPages' => $pagesAvail]);
        }

        if (($allUserSearches + ($accSearches - $account->getSearchJobs())) > $maxSearches) {
            $searchesAvail = $maxSearches - $allUserSearches;
            return new JsonResponse(['limitSearches' => $searchesAvail]);
        }
        //fehlerhafte Eingabe?
        elseif ($accPages === 0 || strpos($accPages, '-') == true) {
            return new JsonResponse(['status' => 'error2']);
        }
        elseif ($accSearches == 0 || strpos($accSearches, '-') == true) {
            return new JsonResponse(['status' => 'error3']);
        }

        else {
            $account = $em->getRepository('AppBundle:Customer')->findOneBy(['id' => $accID]);


            //zählen, wie viele Pages der User hat
            $accountPages = count($em->getRepository('AppBundle:Page')
                ->createQueryBuilder('p')
                ->select('p')
                ->where('p.customer = :id')
                ->andWhere('p.deleted = false')
                ->setParameter('id', $accID)
                ->getQuery()
                ->getResult());


            //wenn mehr Pages als gewünschtes Limit -> Fehler
            if ($accountPages > $accPages) {
                $availPages = $accountPages - $accPages;
                return new JsonResponse(['pagesAvail' => $availPages]);
            }

            //zählen, wie viele Suchen der User hat
            $accountSearches = count($em->getRepository('AppBundle:Search')
                ->createQueryBuilder('s')
                ->select('s')
                ->where('s.customer = :id')
                ->setParameter('id', $accID)
                ->getQuery()
                ->getResult());



            //wenn mehr Suchaufträge als gewünschtes Limit -> Fehler
            if ($accountSearches > $accSearches ) {
                $availSearches = $accountSearches - $accSearches;

                return new JsonResponse(['searchesAvail' => $availSearches]);
            }

            //maximale Suchaufträge und Fanpages für Account in DB schreiben
            /**
             * @var Customer $account
             */
            $account->setMaxPages($accPages);
            $account->setSearchJobs($accSearches);

            $em->flush();

            return new JsonResponse(['status' => 'ok']);
        }
    }

    /**
     * @Route("/delete", name="deleteAccount", options={"expose"=true})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @return JsonResponse
     */
    public function deleteAccountAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Sie haben keine administrativen Rechte.');

        $em = $this->getDoctrine()->getManager();

        //kommt über AJAX
        $accID = $request->request->getInt('accID');

        if ($accID) {

            $userManager = $this->get('fos_user.user_manager');

            $user = $userManager->findUserBy(['id' => $accID]);

            if (!$user) {
                // user not found, generate error
                return new JsonResponse(['status' => 'user']);
            } else {
                $userManager->deleteUser($user);
            }

            return new JsonResponse(['status' => 'ok']);
        } else {
            return new JsonResponse(['status' => 'error']);
        }
    }

    /**
     * @Route("/add", name="addAccount", options={"expose"=true})
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @return JsonResponse
     */
    public function addAccountAction(Request $request)
    {
        //user must be admin to go here
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', null, 'Sie haben keine administrativen Rechte.');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        //get via AJAX
        $accUser = $request->request->get('accUser');
        $accName = $request->request->get('accName');
        $accMail = $request->request->get('accMail');
        $accPages = $request->request->getInt('accPages');
        $accSearches = $request->request->getInt('accSearches');

        $accounts = $em->getRepository('AppBundle:Customer')->findAll();
        foreach ($accounts as $acc) {
            if ($accUser == $acc->getUsername()) {
                return new JsonResponse(['status' => 'userExists']);
            }
        }
        //Userame Check
        if ($accUser == "") {
            return new JsonResponse(['status' => 'userMiss']);
        } elseif (strlen($accUser) < 3) {
            return new JsonResponse(['status' => 'userShort']);
        }

        //Name Check
        if ($accName == "") {
            return new JsonResponse(['status' => 'nameMiss']);
        } elseif (strlen($accName) < 3) {
            return new JsonResponse(['status' => 'nameShort']);
        }

        //Mail Check
        $accounts = $em->getRepository('AppBundle:Customer')->findAll();
        foreach ($accounts as $account) {
            if ($account->getEmail() == $accMail) {
                return new JsonResponse(['status' => 'mailThere']);
            }
        }
        if ($accMail == "") {
            return new JsonResponse(['status' => 'mailMiss']);
        } elseif (!filter_var($accMail, FILTER_VALIDATE_EMAIL)) {
            return new JsonResponse(['status' => 'mailFalse']);
        }


        // maximale Fanpages/Suchen/Accounts pro Installation
        $maxPages = $this->container->getParameter('admin_max_pages');
        $maxSearches = $this->container->getParameter('admin_max_searches');
        $maxAccounts = $this->container->getParameter('admin_max_accounts');

        /*
         * @var Customer $user
         */
        //zählen, wie viele Seiten die User haben
        $allUserPages = 0;
        foreach ($accounts as $user) {
            $allUserPages = $allUserPages + $user->getMaxPages();
        }
        //zählen, wie viele Suchen die User haben
        $allUserSearches = 0;
        foreach ($accounts as $user) {
            $allUserSearches = $allUserSearches + $user->getSearchJobs();
        }

        if (($allUserPages + ($accPages - $user->getMaxPages())) > $maxPages) {
            $pagesAvail = $maxPages - $allUserPages;
            return new JsonResponse(['limitPages' => $pagesAvail]);
        }

        if (($allUserSearches + ($accSearches - $user->getSearchJobs())) > $maxSearches) {
            $searchesAvail = $maxSearches - $allUserSearches;
            return new JsonResponse(['limitSearches' => $searchesAvail]);
        }

        if (count($accounts) >= $maxAccounts) {
            return new JsonResponse(['status' => "accounts"]);
        }

        $pw = $this->generateRandomString();

        $userManager = $this->get('fos_user.user_manager');
        /**
         * @var Customer $user
         */
        $user = $userManager->createUser();
        $user->setUsername($accUser);
        $user->setFullname($accName);
        $user->setEmail($accMail);
        $user->setPlainPassword($pw);
        $user->setEnabled(true);
        $user->setMaxPages($accPages);
        $user->setSearchJobs($accSearches);
        $userManager->updateUser($user);

        $this->container->getParameter('host');

        $mail = (new \Swift_Message('Ihr neuer Digblue-Account'))
            ->setFrom('support@digblue.de')
            ->setTo($accMail)
            ->setBody(
                $this->renderView(
                    'emails/pw.html.twig',
                    array('name' => $accName,
                        'pw' => $pw,
                        'user' => $accUser,
                        'link' => $this->container->getParameter('host'),
                    )
                ),
                'text/html'
            );

        $mailer = $this->get('mailer');
        $mailer->send($mail);

        return new JsonResponse(['status' => 'ok']);

    }

    private function generateRandomString($length = 6)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

}

