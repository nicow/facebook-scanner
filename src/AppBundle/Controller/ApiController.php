<?php
/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 09.08.2017
 * Time: 15:04
 */


namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use AppBundle\Entity\Page;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 * @package AppBundle\Controller
 * @Route("/api")
 */
class ApiController extends Controller
{

    /**
     * @param Request $request
     * @Route("/result.json")
     * @Method("POST")
     *
     * @return JsonResponse
     */
    public function resultAction(Request $request)
    {

        $content = unserialize($request->getContent());

        /**
         * @var ObjectManager $em
         */
        $em = $this->getDoctrine()->getManager();
        $pageRepo = $em->getRepository('AppBundle:Page');
        $job = $em->getRepository('AppBundle:Search')->findOneBy(['term' => $content['search'], 'customer' => $content['customer']]);
        $user = $em->getRepository('AppBundle:Customer')->find($content['customer']);
        $helper = $this->get('app.helper.page');
        if (!$job) {
            return new JsonResponse(['error' => 'missing job'], 400);
        }

        $job->setLastComplete((new \DateTime()));


        //Fehler, wenn Account zu viele Pages hat
        if ($user->getMaxPages() <= $pageRepo->getPagesByUser($user)) {
            return new JsonResponse(['status' => 'error']);
        }

        foreach ($content['data'] as $item) {

            try {
                $page = $pageRepo->findOneBy(['facebookID' => $item['id'], 'customer' => $job->getCustomer()]);
                if (!$page) {
                    $page = $helper->getNewPage($item['id']);
                    $page->setSearch($job);
                }

                if ($page->isDisabled()) {
                    continue;
                }

                /**
                 * @var Customer $user
                 */
                $user = $job->getCustomer();
                $api = $this->get('app.api.city');

                $page = $helper->fillPage($page, $item, $user->getSearchCountries(), $user->isUseResultsWithoutCountry(), $api);

                if (!$page) {
                    continue;
                }
                if (!$page->getId()) {
                    if ($user->getMaxPages() > count($pageRepo->findBy(['customer' => $user]))) {
                        $em->persist($page);
                    } else {
                        unset($page);
                    }

                }

                if (isset($page)) {
                    $page->setCustomer($user);
                }


                $em->flush();
            } catch (\Exception $exception) {
                continue;
            }
        }

        $pages = $em->getRepository('AppBundle:Page')->findAllWithoutName();

        $facebook = $this->get('app.facebook');

        /**
         * @var Page $page
         */
        foreach ($pages as $page) {
            $data = $facebook->getData($page->getFacebookID(), ['name'])->getResult();
            $page->setName($data['name']);
            $page->setCleanName($page->getName());
            $page->setScrapeDate((new \DateTime()));
            $page->setSearch($job);
            $em->flush();
        }


        $em->getRepository('AppBundle:App')->findOneBy(['accesstoken' => $content['access_token']])->setInUse(false);

        $em->flush();

        return new JsonResponse(['status' => 'done', 'access_token' => $content['access_token']]);

    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @Route("/error.json")
     */
    public function errorAction(Request $request)
    {
        if (!$request->query->has('identify')) {
            return new JsonResponse(['error' => 'unauthorized'], 403);
        }

        $allowed_macs = [
            '00:16:3e:01:0d:55',
            '90:1b:0e:de:e3:5e'
        ];

        if (!in_array($request->query->get('identify', null), $allowed_macs)) {
            return new JsonResponse(['error' => 'unauthorized mac'], 403);
        }

        $content = unserialize($request->getContent());

        $em = $this->getDoctrine()->getManager();

        mail("nico.wehmoeller@crea.de", "app " . $content['access_token'] . ' was blocked','error');

        $em->getRepository('AppBundle:App')->findOneBy(['accesstoken' => $content['access_token']])->setBlocked(true);

        $em->flush();

        return new JsonResponse(['status' => 'done']);


    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @Route("/work.json")
     */
    public function offerWorkAction(Request $request)
    {
        if (!$request->query->has('identify')) {
            return new JsonResponse(['error' => 'unauthorized'], 403);
        }

        $allowed_macs = [
            '00:16:3e:01:0d:55',
            '90:1b:0e:de:e3:5e'
        ];

        if (!in_array($request->query->get('identify', null), $allowed_macs)) {
            return new JsonResponse(['error' => 'unauthorized mac'], 403);
        }


        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Search');
        $search = $repo->getNextSearch();

        $repoApp = $em->getRepository('AppBundle:App');
        $apps = $repoApp->findBy(['blocked' => false, 'inUse' => false]);


        if (!$apps || $apps == []) {
            return new JsonResponse(['error' => 'no app available']);
        } else {
            shuffle($apps);
            $search->setLastServed((new \DateTime()));
            $apps[0]->setInUse(true);
            $em->flush();
        }

        return new JsonResponse(['customer' => $search->getCustomer()->getId(), 'search' => $search->getTerm(), 'access_token' => $apps[0]->getAccesstoken()]);

    }
}