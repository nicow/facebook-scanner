<?php
/**
 * Created by PhpStorm.
 * User: nicow
 * Date: 15.08.2017
 * Time: 16:52
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContentController
 * @package AppBundle\Controller
 * @Route("/content")
 */
class ContentController extends Controller
{
    /**
     * @param Request $request
     * @Route("/")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $prepo = $em->getRepository('AppBundle:Post');
        $search = $request->query->get('q', null);
        $searches = $em->getRepository('AppBundle:Search')->findBy(['customer' => $this->getUser()]);
        $lists = $em->getRepository('AppBundle:PageList')->findBy(['customer' => $this->getUser()]);

        $pList = $request->query->getInt('pList') ? $request->query->getInt('pList') : NULL;
        if ($pList != null) {
            $filterList = $em->getRepository("AppBundle:PageList")->findOneBy(['id' => $pList]);
        } else {
            $filterList = null;
        }

        $pSearch = $request->query->getInt('pSearch') ? $request->query->getInt('pSearch') : NULL;
        if ($pSearch != null) {
            $filterSearch = $em->getRepository("AppBundle:Search")->findOneBy(['id' => $pSearch]);
        }else {
            $filterSearch = null;
        }

        $data = $prepo->getFilteredPosts($search, $pList, $pSearch);

        return $this->render('posts/index.html.twig',[
            'posts'=>$data,
            'searches' => $searches,
            'lists' => $lists,
            'filterSearch' => $filterSearch,
            'filterList' => $filterList,

        ]);
    }
}