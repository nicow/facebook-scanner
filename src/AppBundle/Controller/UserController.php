<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use AppBundle\Service\Facebook;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 20.06.2017
 * Time: 10:00
 */

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     * @Route("/profile")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(Request $request)
    {
        return $this->render('user/profile.html.twig', ['countries'=>Facebook::getCountryList()]);
    }

    /**
     * @param Request $request
     * @Route("/updateUser/search", options={"expose"=true})
     * @Method("POST")
     * @return JsonResponse
     */
    public function saveSearchSettingsAction(Request $request)
    {
        //checking for chosen countries
        $countries = $request->request->get('countries', []);
        if (!is_array($countries)) {
            $countries = [];
        }
        $save_country_less_results = $request->request->get('save_country_less_results', false);
        if ($save_country_less_results == "true") {
            $save_country_less_results = true;
        } else {
            $save_country_less_results = false;
        }

        //checking for chosen max pagination
        $maxPagination = $request->request->get('max_pagination');


        $em = $this->getDoctrine()->getManager();
        /**
         * @var Customer $user
         */
        $user = $this->getUser();
        $user->setSearchCountries($countries);
        $user->setUseResultsWithoutCountry($save_country_less_results);
        $user->setMaxPagination($maxPagination);

        $em->flush();

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Method("POST")
     * @Route("/updateUser", options={"expose"=true})
     */
    public function updateUserProfile(Request $request)
    {
        $fullname = $request->request->get('fullname', $this->getUser()->getFullname());
        $email = $request->request->get('email', $this->getUser()->getEmail());

        $manager = $this->get('fos_user.user_manager');
        $user = $manager->findUserByUsername($this->getUser()->getUsername());
        $user->setFullname($fullname);
        $user->setEmail($email);
        $manager->updateUser($user);

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Method("POST")
     * @Route("/changePassword", options={"expose"=true})
     */
    public function changePasswordAction(Request $request)
    {
        $password = $request->request->get('password', null);
        $password_new = $request->request->get('password_new', null);
        $password_new_repeat = $request->request->get('password_new_repeat', null);

        if (!$this->checkPassword($password)) {
            return new JsonResponse(['error' => 'Bitte geben Sie Ihr aktuelles Passwort ein.']);
        } else {

            if (!$password_new || !$password_new_repeat || $password_new == null || $password_new_repeat == null) {
                return new JsonResponse(['error' => 'Die eingegebenen Passwörter stimmen nicht überein. Bitte überprüfen Sie Ihre Eingabe.']);
            }

            $manager = $this->get('fos_user.user_manager');
            /**
             * @var Customer $user
             */
            $user = $this->getUser();
            $user = $manager->findUserByEmail($user->getEmail());
            $user->setPlainPassword($password);
            $manager->updateUser($user);

            return new JsonResponse(['status' => 'ok']);
        }
    }

    private function checkPassword($password)
    {
        $encoder = $this->get('security.encoder_factory');

        $encoder = $encoder->getEncoder($this->getUser());

        if ($encoder->isPasswordValid($this->getUser()->getPassword(), $password, $this->getUser()->getSalt())) {
            return true;
        }

        return false;
    }

}
