<?php

namespace AppBundle\Controller;

#
use AppBundle\Command\SearchCommand;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Page;
use AppBundle\Entity\Search;
use ColourStream\Bundle\CronBundle\Entity\CronJobResult;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 16.06.2017
 * Time: 16:36
 * Class SearchController
 * @package AppBundle\Controller
 * @Route("/search")
 */
class SearchController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/results.json")
     */
    public function resultsAction(Request $request)
    {
        $query = $request->query->get('q', null);
        $type = $request->query->get('type', 'page');
        $after = $request->query->get('after', null);

        $limit = $request->query->get('limit', $this->getParameter("search_default_per_page"));

        $maxPerPage = $this->getParameter('search_max_per_page');

        if ($limit > $maxPerPage) {
            $limit = $maxPerPage;
        }

        $fields = [
            'location{city,country,zip,street}',
            'about',
            'fan_count',
            'is_verified',
            'link',
            'talking_about_count',
            'category',
            'displayed_message_response_time',
            'were_here_count',
            'description',
            'cover{source}',
            'name',
            'phone',
            'impressum',
            'username',
            'website',
            'picture{url}'
        ];

        $results = $helper = $this->get('app.facebook')->search($query, $type, $limit, $fields, $after)->getResult();

        $response = new JsonResponse($results, 200, ['Content-Type' => 'application/json']);

        return $response;

    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/export", options={"expose"=true})
     */
    public function exportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Page');
        $term = $request->query->get('term', null);
        $list = $request->request->get('list', null);
        $search = $request->request->get('search', false);
        $user = $this->getUser();


        if (!$term && !$list && !$search) {
            throw new AccessDeniedException($this->generateUrl('app_search_export'));
        }

        if ($search) {
            $pages = json_decode($request->request->get('pages'), true);
                //first line is empty and therefore deleted
                unset($pages[0]);
             $pages = $repo->findBy(['id' => $pages, 'customer' => $user]);

            $formatedName = strtolower(str_replace(' ', '_', $search));
            $filename = 'search_' . $formatedName . '_export_' . $term . '_' . date('d_m_y_H_i') . '.csv';

        } else {
            $pages = json_decode($request->request->get('pages'), true);
            //first line is empty and therefore deleted
            unset($pages[0]);
            $pages = $repo->findBy(['id' => $pages, 'customer' => $user]);

            $formatedName = strtolower(str_replace(' ', '_', $list));
            $filename = 'list_' . $formatedName . '_export_' . $term . '_' . date('d_m_y_H_i') . '.csv';
        }

        $csv = self::buildCSVFromPage();

        foreach ($pages as $page) {
            $csv .= self::buildCSVFromPage($page);
        }
        $response = new Response($csv, 200);
        $response->headers->add(['Content-Type' => 'text/csv']);
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '";');
        $response->headers->set('Content-length', strlen($csv));

        return $response;

    }

    /**
     * @param Request $request
     * @Route("/", options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function pageSearchAction(Request $request)
    {
        $user = $this->getUser();
        $query = $request->query->get('s', null);
        $em = $this->getDoctrine()->getManager();
        $pageRepo = $em->getRepository('AppBundle:Page');

        if ($request->query->has("list")) {
            $list = $em
                ->getRepository('AppBundle:PageList')
                ->findOneBy(['id' => $request->query->get('list'), 'customer' => $user]
                );
        } else {
            $list = null;
        }

        if ($query) {
            $results = $pageRepo->search($query, $user, $request->query->get('sort', null), $request->query->get('direction', 'asc'), $list);

            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $results, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
            );
        } else {
            if ($list) {
                $results = $pageRepo->search(null, $user, $request->query->get('sort', null), $request->query->get('direction', 'asc'), $list);

                $paginator = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                    $results, /* query NOT result */
                    $request->query->getInt('page', 1)/*page number*/,
                    $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
                );
            } else {
                $pagination = null;
            }
        }

        return $this->render('search/results.html.twig', ['pagination' => $pagination]);

    }

    /**
     * @param Request $request
     * @param Search $search
     * @return JsonResponse
     * @Route("/job/{search}/now", options={"expose"=true})
     */
    public function searchNowAction(Request $request, Search $search)
    {

        $command = new SearchCommand();
        $command->setContainer($this->get('service_container'));

        $input = new ArrayInput([$search->getTerm()]);

        $output = new NullOutput();

        $command->run($input, $output);

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @param Request $request
     * @param Search $search
     * @return JsonResponse
     * @Route("/delete")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repo = $em->getRepository('AppBundle:Search');

        if ($request->query->has('id')) {
            $search = $request->query->getInt('id');

            $search = $repo->find($search);

            /**
             * @var Customer $customer
             */

            $customer = $search->getCustomer();
            if ($customer == $this->getUser()) {

                $searches = $customer->getActivatedPages();
                foreach ($searches as $key => $value) {
                    if ($value == $search->getId()) {
                        unset($searches[$key]);
                    }
                }
                $customer->setActivatedPages($searches);

                $em->remove($search);
                $em->flush();
            }

            return new JsonResponse(['status' => 'ok']);
        } else {
            return new JsonResponse(['status' => 'error']);
        }

    }

    /**
     * @param Request $request
     * @param Search $search
     * @return JsonResponse
     * @return RedirectResponse
     * @Route("/delete/all")
     */
    public function deleteAllAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repo = $em->getRepository('AppBundle:Search');
        $pageRepo = $em->getRepository('AppBundle:Page');

        if ($request->query->has('id')) {
            $search = $request->query->getInt('id');

            $search = $repo->find($search);

            $deleted = $pageRepo->findBySearch($search);

            /**
             * @var Page $page
             */
            foreach ($deleted as $page) {
                $page->setDeleted(true);
            }
            $em->flush();

            /**
             * @var Customer $customer
             */

            $customer = $search->getCustomer();
            if ($customer == $this->getUser()) {


                $searches = $customer->getActivatedPages();
                foreach ($searches as $key => $value) {
                    if ($value == $search->getId()) {
                        unset($searches[$key]);
                    }
                }
                $customer->setActivatedPages($searches);

                $em->remove($search);
                $em->flush();

            }

            return new JsonResponse(['status' => 'ok']);

        } elseif ($request->query->has('pSearch')) {

            $search = $request->query->getInt('pSearch');

            $search = $repo->find($search);

            $deleted = $pageRepo->findBySearch($search);

            /**
             * @var Page $page
             */
            foreach ($deleted as $page) {
                $page->setDeleted(true);
            }
            $em->flush();

            /**
             * @var Customer $customer
             */

            $customer = $search->getCustomer();
            if ($customer == $this->getUser()) {


                $searches = $customer->getActivatedPages();
                foreach ($searches as $key => $value) {
                    if ($value == $search->getId()) {
                        unset($searches[$key]);
                    }
                }
                $customer->setActivatedPages($searches);

                $em->remove($search);
                $em->flush();


            }
            return $this->redirectToRoute('app_search_searchtermoverview');

        } else {
            return new JsonResponse(['status' => 'error']);
        }

    }


    /**
     * @param Request $request
     * @param Page $page
     * @return JsonResponse
     * @Route("/delete/{id}", options={"expose"=true})
     */
    public function deletePageAction(Request $request, Page $page)
    {
        $em = $this->getDoctrine()->getManager();

        if ($page->getCustomer() == $this->getUser()) {

            $page->setDeleted(true);
            $em->flush();

            return new JsonResponse(['status' => 'ok']);
        } else {
            return new JsonResponse(['status' => 'error']);
        }
    }


    /**
     * @param Request $request
     * @Route("/job/new", name="app_search_job_new")
     * @return JsonResponse
     */
    public function newSearchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $repo = $em->getRepository('AppBundle:Search');
        if (count($repo->findBy(['customer' => $this->getUser()])) >= $this->getParameter('search_max_job_per_customer')) {
            return new JsonResponse(['error' => 'Sie können maximal ' . $this->getParameter('search_max_job_per_customer') . ' Suchaufträge anlegen.']);
        }

        $term = $request->request->get('term');
        $search = new Search();
        $search->setCustomer($this->getUser());
        $search->setTerm($term);
        $search->setAfter("NONE");
        $em->persist($search);
        $em->flush();

        return new JsonResponse(['id' => $search->getId(), 'term' => $search->getTerm()]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/job")
     */
    public function searchTermOverviewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Search');
        $cronJobRepo = $this->getDoctrine()->getManager()->getRepository('ColourStreamCronBundle:CronJobResult');

        /**
         * @var CronJobResult $lastCron
         */

        $lastCron = $cronJobRepo->createQueryBuilder('cron_job_result')
            ->select('cron_job_result.runAt')
            ->orderBy('cron_job_result.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$lastCron) {
            $lastCron = false;
        } else {
            $lastCron = $lastCron['runAt'];
        }

        return $this->render('search/index.html.twig', [
            'terms' => $repo->findBy(['customer' => $this->getUser()]),
            'lastCron' => $lastCron
        ]);
    }

    public static function buildCSVFromPage(Page $page = null)
    {
        if (!$page) {
            return implode(';', array_map('utf8_decode', [
                    'Name', 'Kategorie', 'Likes', "Waren hier", "Sprechen hierüber", 'Interaktionsrate', 'Straße', 'Postleitzahl', 'Stadt', 'Land', 'Telefon', 'Suchauftrag', 'Erfasst am'
                ])) . PHP_EOL;
        }

        return implode(';', array_map('utf8_decode', [
                $page->getName(),
                $page->getCategory(),
                number_format($page->getFanCount(), 0, ',', '.'),
                number_format($page->getWereHere(), 0, ',', '.'),
                number_format($page->getTalkingAbout(), 0, ',', '.'),
                $page->getInteractionrate() ? number_format($page->getInteractionrate(), 2, ',', '.') : 'noch nicht berechnet',
                $page->getStreet() ? $page->getStreet() : 'k.A.',
                $page->getZip() ? $page->getZip() : 'k.A.',
                $page->getCity() ? $page->getCity() : 'k.A.',
                $page->getCountry() ? $page->getCountry() : 'k.A.',
                $page->getPhone() ? $page->getPhone() : 'k.A.',
                $page->getSearch() ? $page->getSearch()->getTerm() : 'k.A.',
                $page->getScrapeDate()->format('d.m.Y H:i')
            ])) . PHP_EOL;

    }

    /**
     * @param Request $request
     * @param Search $search
     * @Route("/job/search/result", options={"expose"=true})
     * @return Response
     */
    public function showPagesBySearchAction(Request $request)
    {

        $id = $request->query->get('id', null);
        $user = $this->getUser();

        $view = $request->query->getInt('view', null);

        if (!$id) {
            return $this->redirectToRoute("homepage");
        }

        $em = $this->getDoctrine()->getManager();

        $pageRepo = $em->getRepository('AppBundle:Page');
        $search = $em->getRepository('AppBundle:Search')->find(['id' => $id]);
        $lists = $em->getRepository('AppBundle:PageList')->findBy(['customer' => $user]);

        if (!$search || $search->getCustomer() != $this->getUser()) {
            return $this->redirectToRoute("homepage");
        }

        $pagesInSearch = $pageRepo->findBySearchID($search);
        $searchname = $search->getTerm();


        $paginator = $this->get('knp_paginator');


        if ($view == 1) {

            $pagination = $paginator->paginate(
                $pagesInSearch, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination(),/*limit per page*/
                array('defaultSortFieldName' => 'p.fanCount', 'defaultSortDirection' => 'desc')
            );
        } elseif ($view == 2) {

            $pagination = $paginator->paginate(
                $pagesInSearch, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination(),/*limit per page*/
                array('defaultSortFieldName' => 'p.interactionrate', 'defaultSortDirection' => 'desc')
            );
        } else {
            $pagination = $paginator->paginate(
                $pagesInSearch, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
            );
        }
        return $this->render('search/searchresults.html.twig', [
            'search' => $search,
            'pagination' => $pagination,
            'name' => $searchname,
            'id' => $id,
            'lists' => $lists,
        ]);

    }

    /**
     * @param Request $request
     * @param Search $search
     * @Route("/job/search/filter")
     * @return Response
     */
    public function filterPagesInSearchAction(Request $request)
    {
        $query = $request->query->get('s', null);
        $id = $request->query->get('id', null);
        $user = $this->getUser();

        if (!$id) {
            return $this->redirectToRoute("app_search_showpagesbysearch");
        }

        $em = $this->getDoctrine()->getManager();

        $pageRepo = $em->getRepository('AppBundle:Page');
        $search = $em->getRepository('AppBundle:Search')->find(['id' => $id]);
        $lists = $em->getRepository('AppBundle:PageList')->findBy(['customer' => $user]);
        $sort = $request->query->get('sort', null);
        $direction = $request->query->get('direction', 'asc');
        $new = null;

        if (!$search || $search->getCustomer() != $this->getUser()) {
            return $this->redirectToRoute("homepage");
        }

        if (!$query) {
            return $this->redirectToRoute('app_search_showpagesbysearch');
        }

        if ($query) {

            $query = explode(' ', $query);

            $name = null;
            $category = null;
            $adress = null;
            $about = null;
            $description = null;
            $impressum = null;


            if ($request->query->has('field_name')) {
                $name = true;
            }
            if ($request->query->has('field_cat')) {
                $category = true;
            }
            if ($request->query->has('field_adr')) {
                $adress = true;
            }
            if ($request->query->has('field_about')) {
                $about = true;
            }
            if ($request->query->has('field_descr')) {
                $description = true;
            }
            if ($request->query->has('field_impr')) {
                $impressum = true;
            }

            if ($name || $category || $adress || $about || $description || $impressum) {


                $pList = $request->query->getInt('pList') ? $request->query->getInt('pList') : NULL;
                if ($pList != null) {
                    $filterList = $em->getRepository("AppBundle:PageList")->findOneBy(['id' => $pList]);
                } else {
                    $filterList = null;
                }

                $pSearch = $id;
                $filterSearch = $search;

              //  $count = count($pageRepo->searchAllPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $searchterm, $adress, $about, $description, $impressum, $id)->getResult());
              //  $results = $pageRepo->searchAllPages($query, $this->getUser(), $request->query->get('sort', null), $request->query->get('direction', 'asc'), $name, $category, $searchterm, $adress, $about, $description, $impressum, $id);

                $results = $pageRepo->getFilteredPages($query, $user, $sort, $direction, $name, $category, $adress, $about, $description, $impressum, $pList, $pSearch, $new);

                $paginator = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                    $results, /* query NOT result */
                    $request->query->getInt('page', 1)/*page number*/,
                    $em->getRepository('AppBundle:Customer')->find($user)->getMaxPagination()/*limit per page*/
                );

                $filterMinus = false;

                $count = count($pageRepo->getFilteredPages($query, $user, $sort, $direction, $name, $category, $adress, $about, $description, $impressum, $pList, $pSearch, $new)->getResult());

                $query = implode(' ', $query);

                return $this->render('search/searchresults.html.twig', [
                    'pagination' => $pagination,
                    'id' => $id,
                    'search' => $search,
                    'name' => $query,
                    'pageCount' => $count,
                    'filter_name' => $name,
                    'filter_category' => $category,
                    'filter_adress' => $adress,
                    'filter_about' => $about,
                    'filter_description' => $description,
                    'filter_impressum' => $impressum,
                    'filter_minus' => $filterMinus,
                    'lists' => $lists,
                    'filterList' => $filterList,
                    'filterSearch' => $filterSearch,
                ]);
            } else {
                return $this->redirectToRoute('app_search_showpagesbysearch');
            }
        }
    }

}




