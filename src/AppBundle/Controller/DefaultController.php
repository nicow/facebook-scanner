<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Customer;
use ColourStream\Bundle\CronBundle\Entity\CronJobResult;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {



        $pageRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Page');
        $searchRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Search');
        $cronJobRepo = $this->getDoctrine()->getManager()->getRepository('ColourStreamCronBundle:CronJobResult');
        $listRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:PageList');

        /**
         * @var CronJobResult $lastCron
         */
        $lastCron = $cronJobRepo->createQueryBuilder('cron_job_result')
            ->select('cron_job_result.runAt')
            ->orderBy('cron_job_result.id', 'desc')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$lastCron) {
            $lastCron = false;
        } else {
            $lastCron = $lastCron['runAt'];
        }

        /**
         * @var Customer $user
         */
        $user = $this->getUser();

        $since = $user->getPageSince();

        if (!$since) {
            $since = new \DateTime();
        }

        $this->getDoctrine()->getManager()->flush();

        $activePages = $user->getActivatedPages();
        $showPages = [];
        foreach ($activePages as $page) {
            if($result = $listRepo->find($page)) {
                $showPages[] = $result;
            }
        }

        //check if the user wants to see an overview
        $overview = $user->getSeeOverview();

        $userPageCount = $pageRepo->countAllExceptDeleted($this->getUser());

        return $this->render('default/index.html.twig', [
            'userPageCount' => $userPageCount,
            'fanpages' => [
                'total' => $pageRepo->getAllCount($this->getUser()),
                'since' => $pageRepo->getSinceCount($this->getUser())
            ],
            'listJobs' => $showPages,
            'searches' => $searchRepo->findBy(['customer' => $this->getUser()]),
            'lists' => $listRepo->findBy(['customer' => $this->getUser()]),
            'lastCron' => $lastCron,
            'overview' => $overview,
        ]);

    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/exit", name="app_logout")
     */
    public function exitAction()
    {
        $em = $this->getDoctrine()->getManager();
        $customer = $this->getUser();
        $customer->setPageSince((new \DateTime()));
        $em->flush();

        return $this->redirectToRoute('fos_user_security_logout');
    }

    /**
     * @return Response
     * @Route("/connection")
     */
    public function connectionAction()
    {
        return new Response(time());
    }

    /**
     * @Route("/support", options={"expose"=true})
     * @Method("POST")
     * @return JsonResponse
     */
    public function sendEmailAction(Request $request)
    {
        $daten = $request->request->all();

        $name = $daten['name'];
        $email = $daten['email'];
        $subject = $daten['subject'];
        $message = $daten['message'];

        $notification = (new \Swift_Message('Neue DigBlue Support Anfrage'))
            ->setFrom($this->getParameter('mailer_from_address'))
        ->setTo('system@digblue.de')
        ->setBody(
            $this->renderView('emails/support.html.twig',
                ['name' => $name,
                'email' => $email,
                'subject' => $subject,
                'message' => $message,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'agent' => $_SERVER['HTTP_USER_AGENT']
            ])
            , 'text/html');

        $mailer = $this->get('mailer');
        $success = $mailer->send($notification);

        if ($success == 0) {
            $status = "fail";
        } else {
            $status = "ok";
        }

        return new JsonResponse(["status"=>$status]);
    }


     /**
     *
     * @Route("/update", name="activePageChange", options={"expose":true})
     */

    public function toggleActivatedPagesAction(Request $request)
    {
        /**
         * @var Customer $user
         */
        $user = $this->getUser();

        if ($request->request->has('active')) {

            $activePages = json_decode($request->request->get('active'), true);
            if (!$activePages) {
                $activePages = [];
            }

            $user->setActivatedPages($activePages);


            if ($request->request->has('overview')) {
                if ($request->request->getInt('overview') == 1) {
                    $seeOverview = true;
                    $user->setSeeOverview($seeOverview);
                    $this->getDoctrine()->getManager()->flush();
                } elseif ($request->request->getInt('overview') == 0) {
                    $seeOverview = false;
                    $user->setSeeOverview($seeOverview);
                    $this->getDoctrine()->getManager()->flush();
                }
            }

            $this->getDoctrine()->getManager()->flush();
        }

        return new JsonResponse(['status' => 'ok']);

    }






}
